package be.vdab.learningfever.sweetkitty.orders;

import java.time.LocalDate;
import java.util.*;

public interface OrderDao {
    Order getOrderById(int orderId);
    List<Order> getOrdersByCustomerID(int id);
    List<Order> getOrdersByDate(String date);
    List<Order> getOrdersByDate(LocalDate date);
    void updateOrder(Order order);
    void insertOrder(Order order);
    void deleteOrder(int orderId);
}
