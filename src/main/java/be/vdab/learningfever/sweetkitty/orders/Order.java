package be.vdab.learningfever.sweetkitty.orders;

import be.vdab.learningfever.sweetkitty.exceptions.InvalidDateException;
import be.vdab.learningfever.sweetkitty.exceptions.InvalidNumberException;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Objects;

public class Order {
    public static String ORDER_DATE_TIME_FORMAT = "uuuu-MM-dd";
    private int orderId;
    private LocalDate orderDate;
    private int CustomerId;

    public Order() {
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        if(orderId < 0){
            throw new InvalidNumberException("OrderId can't be negative");
        }
        this.orderId = orderId;
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }
    public String getOrderDateAsString(){
        return getOrderDate().format(
                DateTimeFormatter.ofPattern(ORDER_DATE_TIME_FORMAT)
        );
    }

    public void setOrderDate(LocalDate orderDate) {
        if(ChronoUnit.DAYS.between(orderDate,LocalDate.now())<0){
            throw new InvalidDateException("The date is Invalid");
        }
        this.orderDate = orderDate;
    }
    public void setOrderDate(String orderDate){
        LocalDate ld = LocalDate.parse(orderDate,
                DateTimeFormatter.ofPattern(ORDER_DATE_TIME_FORMAT));
        if(ChronoUnit.DAYS.between(ld,LocalDate.now())<0){
            throw new InvalidDateException("The date is Invalid");
        }
        this.orderDate = ld;
    }


    public int getCustomerId() {
        return CustomerId;
    }

    public void setCustomerId(int customerId) {
        if( customerId <= 0){
            throw new InvalidNumberException("CustomerID can't be Zero or Negative");
        }
        CustomerId = customerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return orderId == order.orderId &&
                CustomerId == order.CustomerId &&
                Objects.equals(orderDate, order.orderDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderId, orderDate, CustomerId);
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderId=" + orderId +
                ", orderDateTime=" + orderDate +
                ", CustomerId=" + CustomerId +
                '}';
    }
}
