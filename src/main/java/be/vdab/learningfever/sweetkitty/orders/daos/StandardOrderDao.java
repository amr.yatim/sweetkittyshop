package be.vdab.learningfever.sweetkitty.orders.daos;

import be.vdab.learningfever.sweetkitty.facade.JdbcFacade;
import be.vdab.learningfever.sweetkitty.orders.Order;
import be.vdab.learningfever.sweetkitty.orders.OrderDao;
import be.vdab.learningfever.sweetkitty.orders.exceptions.OrderException;
import be.vdab.learningfever.sweetkitty.util.EntityGetter;
import be.vdab.learningfever.sweetkitty.util.QueryHelper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class StandardOrderDao implements OrderDao, EntityGetter<Order> {
    public static final String ORDER_DATE_TIME_FORMAT = Order.ORDER_DATE_TIME_FORMAT;
    private JdbcFacade facade;

    public StandardOrderDao() {
        this(new JdbcFacade());
    }

    public StandardOrderDao(JdbcFacade facade) {
        this.setFacade(facade);
    }

    public JdbcFacade getFacade() {
        return facade;
    }

    public void setFacade(JdbcFacade facade) {
        this.facade = facade;
    }

    @Override
    public Order getOrderById(int orderId) {
        try(
                ResultSet set = getFacade().executeQuery(
                        QueryHelper.getQuery(orderId,"Orders","OrderID")
                    )
                ){
            if(set.next()){
                return getEntityByResultSet(set);
            }
        }catch (SQLException sqle){
            throw new OrderException(sqle.getMessage());
        }

        return null;
    }

    @Override
    public List<Order> getOrdersByCustomerID(int id) {
        return QueryHelper.executeQuery(
                    QueryHelper.getQuery(id,"Orders","CustomerID"),
                    getFacade(),
                    this::getEntityByResultSet
                );
    }

    @Override
    public List<Order> getOrdersByDate(String date) {
        return QueryHelper.executeQuery(
                QueryHelper.getQueryDate(date,"Orders","OrderDate"),
                getFacade(),
                this::getEntityByResultSet
        );
    }

    @Override
    public List<Order> getOrdersByDate(LocalDate date) {
        return QueryHelper.executeQuery(
                QueryHelper.getQueryDate(
                        date.format(DateTimeFormatter.ofPattern(ORDER_DATE_TIME_FORMAT)),
                        "Orders",
                        "OrderDate"),
                getFacade(),
                this::getEntityByResultSet
        );
    }

    @Override
    public void updateOrder(Order order) {
        if(order == null){
            throw new OrderException("update order isn't possible by null");
        }
        getFacade().executeUpdate(
                "UPDATE Orders Set " +
                        "CustomerId="+order.getCustomerId()+", " +
                        "OrderDate='"+order.getOrderDateAsString()+"' " +
                        "WHERE OrderID="+order.getOrderId()+";"
        );
    }

    @Override
    public void insertOrder(Order order) {
        if(order == null){
            throw new OrderException("insert order isn't possible by null");
        }
        getFacade().executeUpdate(
                "INSERT INTO Orders(OrderDate,CustomerID) " +
                        "VALUES(" +
                        "'"+order.getOrderDateAsString()+"' ," +
                        ""+order.getCustomerId()+");"
        );
    }

    @Override
    public void deleteOrder(int orderId) {
        if(orderId<=0){
            throw new OrderException("delete order isn't possible by zero or negative numbers");
        }
        getFacade().executeUpdate("DELETE FROM Orders WHERE OrderID="+orderId+";");
    }

    @Override
    public Order getEntityByResultSet(ResultSet set) {
        Order order = new Order();
        try {
            order.setOrderId(set.getInt("OrderID"));
            order.setCustomerId(set.getInt("CustomerID"));
            order.setOrderDate(set.getDate("OrderDate").toLocalDate());
            return order;
        }catch (SQLException sqle){
            throw new OrderException(sqle.getMessage());
        }
    }
}
