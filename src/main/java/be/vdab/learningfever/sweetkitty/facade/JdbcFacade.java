package be.vdab.learningfever.sweetkitty.facade;

import java.io.*;
import java.sql.*;
import java.time.format.ResolverStyle;
import java.util.ArrayList;
import java.util.Properties;

public class JdbcFacade implements AutoCloseable {

    public static final String DEFAULT_PROPERTIES_PATH = "src/main/resources/mainInfo.properties";
    public static final int TYPE_STANDARD = ResultSet.TYPE_FORWARD_ONLY;
    public static final int TYPE_SCROLL = ResultSet.TYPE_SCROLL_INSENSITIVE;
    public static final int TYPE_SCROLL_SYNCED = ResultSet.TYPE_SCROLL_SENSITIVE;
    public static final int CONCUR_READ_ONLY = ResultSet.CONCUR_READ_ONLY;
    public static final int CONCUR_UPDATABLE = ResultSet.CONCUR_UPDATABLE;

    private Connection connect;
    private Properties properties;
    private ArrayList<Statement> statements;
    private ArrayList<ResultSet> resultSets;

    public JdbcFacade() {
        this(DEFAULT_PROPERTIES_PATH);
    }

    public JdbcFacade(String propertiesPath) {
        loadProperties(propertiesPath);
        createConnection();
        statements = new ArrayList<>();
        resultSets = new ArrayList<ResultSet>();
    }

    private void createConnection(){
        try{
            connect = DriverManager.getConnection(getURL(),getUser(),getPassword());
        }catch(SQLException e){
            e.printStackTrace();
        }
    }

    private void loadProperties(String path){
        try(
                FileInputStream fis = new FileInputStream(path)
        ){
            properties = new Properties();
            properties.load(fis);
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    public Connection getConnect() {
        return connect;
    }

    private void setConnect(Connection connect) {
        this.connect = connect;
    }

    Properties getProperties() {
        return properties;
    }

    private void setProperties(Properties properties) {
        this.properties = properties;
    }

    public String getURL(){
        return getProperties().getProperty("jdbc.url");
    }

    public String getUser(){
        return getProperties().getProperty("jdbc.user");
    }

    public String getPassword(){
        return getProperties().getProperty("jdbc.psw");
    }

    public void setAutoCommit(){
        try {
            getConnect().setAutoCommit(false);
        }catch (SQLException sqle){
            sqle.printStackTrace();
        }
    }

    public void removeAutoCommit(){
        try{
            getConnect().commit();
            getConnect().setAutoCommit(true);
        }catch(SQLException sqle){
            rollBack();
        }
    }

    public void rollBack(){
        try{
            getConnect().rollback();
        }catch (SQLException sqle){
            sqle.printStackTrace();
        }
    }

    public void commitCurrentStatements(){
        try {
            getConnect().commit();
        }catch (SQLException sqle){
            rollBack();
            sqle.printStackTrace();
        }
    }

    public Statement getStatement(){
        try {

            Statement s = connect.createStatement(TYPE_STANDARD,ResultSet.CONCUR_READ_ONLY);
            statements.add(s);
            return s;
        }catch (SQLException e){
            throw new RuntimeException(e.getMessage());
        }
    }

    public Statement getStatement(int restultSetType, int resultSetConcurent){
        try {
            Statement s = connect.createStatement(restultSetType,resultSetConcurent);
            statements.add(s);
            return s;
        }catch (SQLException e){
            throw new RuntimeException(e.getMessage());
        }
    }

    public ResultSet executeQuery(String query){
        try {
            ResultSet set =  getStatement().executeQuery(query);
            resultSets.add(set);
            return set;
        }catch (SQLException e){
            throw new RuntimeException(e.getMessage());
        }
    }

    public ResultSet executeQueryNavigable(String query){
        try {
            ResultSet set = getStatement(TYPE_SCROLL,CONCUR_READ_ONLY).executeQuery(query);
            resultSets.add(set);
            return set;
        }catch (SQLException e){
            throw new RuntimeException(e.getMessage());
        }
    }

    public ResultSet executeQueryNavigableUpdatable(String query){
        try {
            ResultSet set = getStatement(TYPE_SCROLL,CONCUR_UPDATABLE).executeQuery(query);
            resultSets.add(set);
            return set;
        }catch (SQLException e){
            throw new RuntimeException(e.getMessage());
        }
    }

    public int executeUpdate(String sql){
        try{
            return getStatement().executeUpdate(sql);
        }catch (SQLException e){
            throw new RuntimeException(e);
        }
    }

    @Override
    public void close() throws Exception {
        for (Statement s : statements) {
            s.close();
        }
        for (ResultSet set: resultSets) {
            set.close();
        }
        connect.close();
    }
}
