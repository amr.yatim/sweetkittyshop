package be.vdab.learningfever.sweetkitty.varieties.exceptions;

public class VarietyException extends RuntimeException{
    public VarietyException() {
    }

    public VarietyException(String message) {
        super(message);
    }

    public VarietyException(String message, Throwable cause) {
        super(message, cause);
    }

    public VarietyException(Throwable cause) {
        super(cause);
    }

    public VarietyException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
