package be.vdab.learningfever.sweetkitty.varieties;

import java.util.List;

public interface VarietyDao {
    Variety getVarietyById(int varietyId);
    List<Variety> getVarietiesByName(String name);
    List<Variety> getVarietiesByUnitPrice(double unitPrice);
    void updateVarieties(Variety variety);
    void insertVariety(Variety variety);
    void deleteVariety(int id);
}
