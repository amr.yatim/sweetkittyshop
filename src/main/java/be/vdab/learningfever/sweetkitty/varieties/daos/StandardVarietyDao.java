package be.vdab.learningfever.sweetkitty.varieties.daos;

import be.vdab.learningfever.sweetkitty.facade.JdbcFacade;
import be.vdab.learningfever.sweetkitty.util.EntityGetter;
import be.vdab.learningfever.sweetkitty.util.QueryHelper;
import be.vdab.learningfever.sweetkitty.varieties.Variety;
import be.vdab.learningfever.sweetkitty.varieties.VarietyDao;
import be.vdab.learningfever.sweetkitty.varieties.exceptions.VarietyException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import static be.vdab.learningfever.sweetkitty.util.QueryHelper.*;

public class StandardVarietyDao implements VarietyDao, EntityGetter<Variety> {
    private JdbcFacade facade;

    public StandardVarietyDao() {
        this(new JdbcFacade());
    }

    public StandardVarietyDao(JdbcFacade facade) {
        this.setFacade(facade);
    }

    public JdbcFacade getFacade() {
        return facade;
    }

    public void setFacade(JdbcFacade facade) {
        this.facade = facade;
    }

    @Override
    public Variety getEntityByResultSet(ResultSet set) {
        Variety type = new Variety();
        try{
            type.setVarietyId(set.getInt("VarietyID"));
            type.setVarietyName(set.getString("Name"));
            type.setDescription(set.getString("Description"));
            type.setUnitPrice(set.getDouble("Price"));
            return type;
        }catch (SQLException sqle){
            throw new VarietyException(sqle.getMessage());
        }
    }

    @Override
    public Variety getVarietyById(int varietyId) {
        try(
                ResultSet set = getFacade().executeQuery(
                        QueryHelper.getQuery(varietyId,"Varieties","VarietyId"))
                ){
            if(set.next()){
                return getEntityByResultSet(set);
            }
        }catch (SQLException sqle){
            throw new VarietyException(sqle.getMessage());
        }
        return null;
    }

    @Override
    public List<Variety> getVarietiesByName(String name) {
        return executeQuery(
                getQuery(name,"Varieties","Name"),
                getFacade(),
                this::getEntityByResultSet
        );
    }

    @Override
    public List<Variety> getVarietiesByUnitPrice(double unitPrice) {
        return executeQuery(
                getQuery(unitPrice,"Varieties","Price"),
                getFacade(),
                this::getEntityByResultSet
        );
    }

    @Override
    public void updateVarieties(Variety variety) {
        if(variety == null){
            throw new VarietyException("insert variety isn't possible by null");
        }
        getFacade().executeUpdate(
                "UPDATE Varieties SET " +
                        "Name = '"+variety.getVarietyName()+"' ," +
                        "Description = '"+variety.getDescription()+"' ," +
                        "Price = "+variety.getUnitPrice()+" " +
                        "WHERE VarietyID = "+variety.getVarietyId()+" ;"
        );
    }

    @Override
    public void insertVariety(Variety variety) {
        if(variety == null){
            throw new VarietyException("insert variety isn't possible by null");
        }
        getFacade().executeUpdate(
                "INSERT INTO Varieties(Name,Description,Price) " +
                        "VALUES( " +
                        "'"+variety.getVarietyName()+"', " +
                        "'"+variety.getDescription()+"' ," +
                        variety.getUnitPrice()+" );"
        );
    }

    @Override
    public void deleteVariety(int id) {
        if(id<=0){
            throw new VarietyException("delete variety isn't possible by zero or negative number");
        }
        getFacade().executeUpdate(
                "DELETE FROM Varieties WHERE VarietyID="+id+";"
        );
    }
}
