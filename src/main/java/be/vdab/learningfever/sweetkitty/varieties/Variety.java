package be.vdab.learningfever.sweetkitty.varieties;

import be.vdab.learningfever.sweetkitty.exceptions.InvalidDateException;
import be.vdab.learningfever.sweetkitty.exceptions.InvalidLengthStringException;
import be.vdab.learningfever.sweetkitty.exceptions.InvalidNumberException;
import be.vdab.learningfever.sweetkitty.exceptions.NullValueException;

import java.util.Objects;

public class Variety {
    private int varietyId;
    private String varietyName;
    private String description;
    private double unitPrice;

    public Variety() {
    }

    public int getVarietyId() {
        return varietyId;
    }

    public void setVarietyId(int varietyId) {
        if(varietyId < 0){
            throw new InvalidNumberException("Variety id can't be negative");
        }
        this.varietyId = varietyId;
    }

    public String getVarietyName() {
        return varietyName;
    }

    public void setVarietyName(String varietyName) {
        if(varietyName == null){
            throw new NullValueException("variety name can't be null");
        }
        if(varietyName.length()<3 || varietyName.length()>45){
            throw new InvalidLengthStringException("variety name must contain more than 3 characters and less than 45 characters");
        }
        this.varietyName = varietyName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        if(description == null){
            throw new NullValueException("variety description can't be null");
        }
        this.description = description;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        if(unitPrice <= 0){
            throw new InvalidNumberException("variety unit price can't be zero or negative");
        }
        this.unitPrice = unitPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Variety variety = (Variety) o;
        return getVarietyId() == variety.getVarietyId() &&
                Double.compare(variety.unitPrice, unitPrice) == 0 &&
                getVarietyName().equalsIgnoreCase(variety.getVarietyName()) &&
                getDescription().equalsIgnoreCase(variety.getDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(varietyId, varietyName, description, unitPrice);
    }

    @Override
    public String toString() {
        return "Variety{" +
                "varietyId=" + varietyId +
                ", varietyName='" + varietyName + '\'' +
                ", description='" + description + '\'' +
                ", unitPrice=" + unitPrice +
                '}';
    }
}
