package be.vdab.learningfever.sweetkitty.addreeses.daos;

import be.vdab.learningfever.sweetkitty.addreeses.Address;
import be.vdab.learningfever.sweetkitty.addreeses.AddressDao;
import be.vdab.learningfever.sweetkitty.addreeses.exception.AddressException;
import be.vdab.learningfever.sweetkitty.cutomers.Customer;
import be.vdab.learningfever.sweetkitty.cutomers.exceptions.CustomerException;
import be.vdab.learningfever.sweetkitty.facade.JdbcFacade;
import be.vdab.learningfever.sweetkitty.util.EntityGetter;
import be.vdab.learningfever.sweetkitty.util.QueryHelper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StandardAddressDoa implements AddressDao, EntityGetter<Address> {

    private JdbcFacade facade ;

    public StandardAddressDoa() {
        this(new JdbcFacade());
    }

    public StandardAddressDoa(JdbcFacade facade) {
        this.setFacade(facade);
    }

    public JdbcFacade getFacade() {
        return facade;
    }

    public void setFacade(JdbcFacade facade) {
        this.facade = facade;
    }

    @Override
    public Address getAddressById(int id) {
        try(
                ResultSet result = facade.executeQuery(QueryHelper.getQuery(id,"Addresses","AddressID"))
                ){
            if(result.next()){
                return getEntityByResultSet(result);
            }

        }catch (SQLException sqle){
            throw new AddressException(sqle.getMessage());
        }
        return null;
    }

    @Override
    public List<Address> getAddressesByZipCode(String zipCode) {
        return QueryHelper.executeQuery(
                QueryHelper.getQuery(zipCode,"Addresses","ZipCode"),
                getFacade(),
                this::getEntityByResultSet);
    }

    @Override
    public List<Address> getAddressesByCity(String city) {
        return QueryHelper.executeQuery(
                QueryHelper.getQuery(city,"Addresses","City"),
                getFacade(),
                this::getEntityByResultSet);
    }

    @Override
    public List<Address> getAddressesByStreet(String street) {
        return QueryHelper.executeQuery(
                QueryHelper.getQuery(street,"Addresses","Street"),
                getFacade(),
                this::getEntityByResultSet);
    }

    @Override
    public void updateAddress(Address address){
        if(address == null){
            throw new AddressException("The Address in the update can't be NULL");
        }

        getFacade().executeUpdate(
                "UPDATE Addresses SET " +
                        "Street='"+address.getStreet()+"',"+
                        "HouseNumber="+address.getHouseNumber()+"," +
                        "Bus='"+address.getBus()+"'," +
                        "ZipCode='"+address.getZipCode()+"'," +
                        "City='"+address.getCity()+"'," +
                        "Country='"+address.getCountry()+"'" +
                        "WHERE AddressID="+address.getAddressID()+";"
        );
    }

    @Override
    public void insertAddress(Address address){
        if(address == null){
            throw new AddressException("The Address in the insert can't be NULL");
        }
        getFacade().executeUpdate(
          "INSERT INTO Addresses(Street,HouseNumber,Bus,ZipCode,City,Country) " +
                  "VALUES('"+address.getStreet()+"'," +
                  +address.getHouseNumber()+"," +
                  "'"+address.getBus()+"'," +
                  "'"+address.getZipCode()+"'," +
                  "'"+address.getCity()+"'," +
                  "'"+address.getCountry()+"');"
        );
    }


    @Override
    public void deleteById(int id){
        if(id <= 0){
            throw new AddressException("The Address in delete can't be Zero or Negative");
        }
        getFacade().executeUpdate("" +
                "DELETE FROM Addresses WHERE " +
                "AddressID="+id+" ;"
        );
    }

    @Override
    public Address getEntityByResultSet(ResultSet set) {
        try {
            Address ad = new Address(
                    set.getString("Street"),
                    set.getInt("HouseNumber"),
                    set.getString("Bus"),
                    set.getString("ZipCode"),
                    set.getString("City"),
                    set.getString("Country")
            );
            ad.setAddressID(set.getInt("AddressID"));
            return ad;
        }catch (SQLException sqle){
            throw new AddressException(sqle.getMessage());
        }
    }
}
