package be.vdab.learningfever.sweetkitty.addreeses;

import be.vdab.learningfever.sweetkitty.exceptions.InvalidLengthStringException;
import be.vdab.learningfever.sweetkitty.exceptions.InvalidNumberException;
import be.vdab.learningfever.sweetkitty.exceptions.NullValueException;

import java.io.Serializable;
import java.util.Objects;

public class Address implements Serializable {

    private int addressID ;
    private String street;
    private int houseNumber ;
    private String bus;
    private String zipCode;
    private String city;
    private String country;

    public Address() {
    }

    public Address( String street, int houseNumber, String bus, String zipCode, String city, String country) {
        setAddressID(0);
        setStreet(street);
        setHouseNumber(houseNumber);
        setBus(bus);
        setZipCode(zipCode);
        setCity(city);
        setCountry(country);
    }

    public int getAddressID() {
        return addressID;
    }

    public void setAddressID(int addressID) {
        if( addressID < 0) {
            throw new InvalidNumberException("The Address ID can't be Zero or Negative");
        }
        this.addressID = addressID;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        if( street == null){
            throw new NullValueException("Street name can't be Null");
        }
        if( street.length() < 3){
            throw new InvalidLengthStringException("Street Name can't must contain more than 2 characters");
        }
        this.street = street;
    }

    public int getHouseNumber() {
        return houseNumber;
    }

    public void setHouseNumber(int houseNumber) {
        if(houseNumber <= 0){
            throw new InvalidNumberException("The house number can't be ZERO or Negative number");
        }
        this.houseNumber = houseNumber;
    }

    public String getBus() {
        return bus;
    }

    public void setBus(String bus) {
        if( bus != null && ( bus.length() <= 0 || bus.length() > 5 ) ){
            throw new InvalidLengthStringException("The House Bus can't contains Zero or more than 5 characters");
        }
        this.bus = bus;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        if(zipCode != null && ( zipCode.length() <= 0 || zipCode.length() > 10)){
            throw new InvalidLengthStringException("The ZipCode can't contains Zero or more than 10 characters");
        }
        this.zipCode = zipCode;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        if(city == null){
            throw new NullValueException("The City can't be null");
        }
        if( city.length() <= 0 || city.length() > 150 ){
            throw new InvalidLengthStringException("The City can't be Empty or contains more 150 characters");
        }
        this.city = city;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        if( country == null ){
            throw new NullValueException("The Country can't be Null");
        }
        if( country.length() <=0 || country.length() > 100){
            throw new InvalidLengthStringException("The Country can't empty or contains more than 100 characters");
        }
        this.country = country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return addressID == address.addressID &&
                houseNumber == address.houseNumber &&
                Objects.equals(street, address.street) &&
                Objects.equals(bus, address.bus) &&
                Objects.equals(zipCode, address.zipCode) &&
                Objects.equals(city, address.city) &&
                Objects.equals(country, address.country);
    }

    @Override
    public int hashCode() {
        return Objects.hash(addressID, street, houseNumber, bus, zipCode, city, country);
    }

    @Override
    public String toString() {
        return "Address{" +
                "addressID=" + addressID +
                ", street='" + street + '\'' +
                ", houseNumber=" + houseNumber +
                ", bus='" + bus + '\'' +
                ", zipCode='" + zipCode + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                '}';
    }
}
