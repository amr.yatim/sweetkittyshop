package be.vdab.learningfever.sweetkitty.addreeses;

import java.util.*;

public interface AddressDao {
    Address getAddressById(int id);
    List<Address> getAddressesByZipCode(String zipCode);
    List<Address> getAddressesByCity(String city);
    List<Address> getAddressesByStreet(String street);
    void updateAddress(Address address);
    void insertAddress(Address address);
    void deleteById(int id);
}
