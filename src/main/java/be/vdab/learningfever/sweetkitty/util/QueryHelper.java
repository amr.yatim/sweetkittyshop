package be.vdab.learningfever.sweetkitty.util;

import be.vdab.learningfever.sweetkitty.cutomers.Customer;
import be.vdab.learningfever.sweetkitty.cutomers.exceptions.CustomerException;
import be.vdab.learningfever.sweetkitty.facade.JdbcFacade;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class QueryHelper {
    public static String getQuery(String searchValue,String tableName,String columnName){
        if(tableName == null || columnName == null){
            throw new RuntimeException("The TableName or ColumnName can't be NULL");
        }
        if(searchValue == null){
            return "SELECT * FROM "+tableName+" WHERE "+columnName+" is NULL;";
        }else{
            return "SELECT * FROM "+tableName+" WHERE UCASE("+columnName+") = "+"UCASE('"+searchValue+"');";
        }
    }
    public static String getQuery(int searchValue,String tableName,String columnName){
        if(tableName == null || columnName == null){
            throw new RuntimeException("The TableName or ColumnName can't be NULL");
        }
        return "SELECT * FROM "+tableName+" WHERE "+columnName+ " = "+searchValue+";";

    }
    public static String getQuery(double searchValue,String tableName,String columnName){
        if(tableName == null || columnName == null){
            throw new RuntimeException("The TableName or ColumnName can't be NULL");
        }
        return "SELECT * FROM "+tableName+" WHERE "+columnName+ " = "+searchValue+";";

    }
    public static String getQueryDate(String searchValue,String tableName,String columnName){
        if(tableName == null || columnName == null){
            throw new RuntimeException("The TableName or ColumnName can't be NULL");
        }
        return "SELECT * FROM "+tableName+" WHERE "+columnName+ " LIKE '"+searchValue+"%';";

    }
    public static <T> List<T> executeQuery(String sql,JdbcFacade facade,EntityGetter<T> entity){
        try(
                ResultSet result = facade.executeQuery(sql)
        ){
            List<T> ListEntities = new ArrayList<>();
            while(result.next()){
                ListEntities.add(entity.getEntityByResultSet(result));
            }
            return ListEntities;
        }catch(SQLException sqle){
            throw new RuntimeException(sqle.getMessage());
        }
    }

}
