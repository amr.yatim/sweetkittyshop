package be.vdab.learningfever.sweetkitty.util;

import java.sql.*;

public interface EntityGetter <T>{
    T getEntityByResultSet(ResultSet set);
}
