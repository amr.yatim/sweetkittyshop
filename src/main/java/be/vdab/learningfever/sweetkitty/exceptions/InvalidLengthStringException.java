package be.vdab.learningfever.sweetkitty.exceptions;

public class InvalidLengthStringException extends RuntimeException {
    public InvalidLengthStringException() {
    }

    public InvalidLengthStringException(String message) {
        super(message);
    }

    public InvalidLengthStringException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidLengthStringException(Throwable cause) {
        super(cause);
    }

    public InvalidLengthStringException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
