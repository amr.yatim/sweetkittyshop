package be.vdab.learningfever.sweetkitty.exceptions;

public class InvalidCharacters extends RuntimeException{
    public InvalidCharacters() {
    }

    public InvalidCharacters(String message) {
        super(message);
    }

    public InvalidCharacters(String message, Throwable cause) {
        super(message, cause);
    }

    public InvalidCharacters(Throwable cause) {
        super(cause);
    }

    public InvalidCharacters(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
