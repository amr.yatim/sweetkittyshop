package be.vdab.learningfever.sweetkitty.cutomers;

import java.util.*;

public interface CustomerDao {
    Customer getCustomerById(int CustomerID);
    List<Customer> getCustomersByFirstName(String firstName);
    List<Customer> getCustomersByLastName(String LastName);
    List<Customer> getCustomersByEmail(String Email);
    List<Customer> getCustomersByTelephone(String telephone);
    List<Customer> getCustomersByAddressId(int addressID);
    void updateCustomer(Customer customer);
    void insertCustomer(Customer customer);
    void deleteCustomerById(int id);
}
