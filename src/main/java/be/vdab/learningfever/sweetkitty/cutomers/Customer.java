package be.vdab.learningfever.sweetkitty.cutomers;

import be.vdab.learningfever.sweetkitty.cutomers.exceptions.InvalidEmailException;
import be.vdab.learningfever.sweetkitty.exceptions.InvalidCharacters;
import be.vdab.learningfever.sweetkitty.exceptions.InvalidLengthStringException;
import be.vdab.learningfever.sweetkitty.exceptions.InvalidNumberException;
import be.vdab.learningfever.sweetkitty.exceptions.NullValueException;

import java.io.Serializable;
import java.util.Objects;

public class Customer implements Serializable {

    private int customerID ;
    private String firstName;
    private String LastName;
    private String email;
    private String telephone;
    private int addressID ;

    public Customer() {
    }

    public Customer(String firstName, String lastName, String email, String telephone, int addressID) {
        this.firstName = firstName;
        LastName = lastName;
        this.email = email;
        this.telephone = telephone;
        this.addressID = addressID;
    }

    public int getCustomerID() {
        return customerID;
    }

    public void setCustomerID(int customerID) {
        if(customerID < 0){
            throw new InvalidNumberException("Customer ID can't be negative");
        }
        this.customerID = customerID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        if (firstName == null) {
            throw new NullValueException("First Name can't be NULL");
        }
        if(firstName.length() < 3 || firstName.length() > 45){
            throw  new InvalidLengthStringException("First Name can't contain less than characters or more than 45 character");
        }
        this.firstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        if (lastName == null) {
            throw new NullValueException("Last Name can't be NULL");
        }
        if(lastName.length() < 3 || lastName.length() > 45){
            throw  new InvalidLengthStringException("Last Name can't contain less than characters or more than 45 character");
        }
        this.LastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        if (email == null) {
            throw new NullValueException("E-mail can't be NULL");
        }
        if(!checkEmailCharacters(email)){
            throw new InvalidEmailException("The e-mail contains illegal characters");
        }

        this.email = email;
    }

    private boolean checkEmailCharacters(String email){
        int indexAt = email.indexOf('@');
        if( indexAt < 0 ){
            return false;
        }
        int indexPoint = email.lastIndexOf('.');
        if( indexPoint < 0){
            return false;
        }
        if(indexAt > indexPoint){
            return false;
        }
        String temp = email.substring(0,indexAt);
        if( temp.length() < 6 ){
            throw new InvalidLengthStringException("Email length before @ must contain more the 6 characters");
        }
        if(!temp.matches("^[a-zA-Z][A-Za-z0-9_.]+[a-zA-Z]$")){
            throw new InvalidCharacters("Email Before @ can't contain illegal characters");
        }
        temp = email.substring(indexAt+1,indexPoint);
        if( temp.length() < 3 ){
            throw new InvalidLengthStringException("Email length After @ must contain more the 3 characters");
        }
        if(!temp.matches("^[a-zA-Z][A-Za-z0-9_.]+[a-zA-Z]$")){
            throw new InvalidCharacters("Email can't contain illegal characters");
        }
        temp = email.substring(indexPoint+1);
        if( temp.length() < 2 ){
            throw new InvalidLengthStringException("Email length After Point(.) must contain more the 1 characters");
        }
        if(!temp.matches("^[a-zA-Z][A-Za-z0-9_.]+[a-zA-Z]$")){
            throw new InvalidCharacters("Email can't contain illegal characters");
        }
        return true;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        if(telephone == null){
            throw new NullValueException("Telephone can't be NULL");
        }
        if(telephone.length() < 7 || telephone.length() > 16){
            throw new InvalidLengthStringException("Telephone must contains between 7 and 16 characters");
        }
        if(!checkIllegalTelephoneCharacter(telephone)){
            throw new InvalidCharacters("Telephone can't contains illegal characters");
        }
        this.telephone = telephone;
    }

    private boolean checkIllegalTelephoneCharacter(String telephone){

        if(!telephone.matches("[0-9]+")){
            return false;
        }
        return true;
    }

    public int getAddressID() {
        return addressID;
    }

    public void setAddressID(int addressID) {
        if(addressID <= 0){
            throw new InvalidNumberException("Address id can't be Zero or negative");
        }
        this.addressID = addressID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return getCustomerID() == customer.getCustomerID() &&
                getAddressID() == customer.getAddressID()&&
                getFirstName().equalsIgnoreCase(customer.getFirstName()) &&
                getLastName().equalsIgnoreCase(customer.getLastName()) &&
                getEmail().equalsIgnoreCase(customer.getEmail()) &&
                getTelephone().equalsIgnoreCase(customer.getTelephone());
    }

    @Override
    public int hashCode() {
        return Objects.hash(customerID, firstName, LastName, email, telephone, addressID);
    }

    @Override
    public String toString() {
        return "Customer{" +
                "customerID=" + customerID +
                ", firstName='" + firstName + '\'' +
                ", LastName='" + LastName + '\'' +
                ", email='" + email + '\'' +
                ", telephone='" + telephone + '\'' +
                ", addressID=" + addressID +
                '}';
    }
}
