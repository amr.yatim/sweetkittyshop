package be.vdab.learningfever.sweetkitty.cutomers.daos;


import be.vdab.learningfever.sweetkitty.cutomers.*;
import be.vdab.learningfever.sweetkitty.cutomers.exceptions.CustomerException;
import be.vdab.learningfever.sweetkitty.facade.JdbcFacade;
import be.vdab.learningfever.sweetkitty.util.*;

import java.sql.*;
import java.util.List;

public class StandardCustomerDao implements CustomerDao, EntityGetter<Customer> {

    private JdbcFacade facade;

    public StandardCustomerDao() {
        this(new JdbcFacade());
    }

    public StandardCustomerDao(JdbcFacade facade) {
        this.setFacade(facade);
    }

    public JdbcFacade getFacade() {
        return facade;
    }

    public void setFacade(JdbcFacade facade) {
        this.facade = facade;
    }

    @Override
    public Customer getCustomerById(int id) {
        try(
                ResultSet result = facade.executeQuery(QueryHelper.getQuery(id,"Customers","CustomerID"))
        ){
            if(result.next()){
                System.out.println("Done");
                return getEntityByResultSet(result);
            }

        }catch (SQLException sqle){
            throw new CustomerException(sqle.getMessage());
        }
        return null;
    }

    @Override
    public List<Customer> getCustomersByFirstName(String firstName) {
        return QueryHelper.executeQuery(
                QueryHelper.getQuery(firstName,"Customers","FirstName"),
                getFacade(),
                this::getEntityByResultSet);
    }

    @Override
    public List<Customer> getCustomersByLastName(String lastName) {
        return QueryHelper.executeQuery(
                QueryHelper.getQuery(lastName,"Customers","LastName"),
                getFacade(),
                this::getEntityByResultSet);
    }

    @Override
    public List<Customer> getCustomersByEmail(String email) {
        return QueryHelper.executeQuery(
                QueryHelper.getQuery(email,"Customers","Email"),
                getFacade(),
                this::getEntityByResultSet);
    }

    @Override
    public List<Customer> getCustomersByTelephone(String telephone) {
        return QueryHelper.executeQuery(
                QueryHelper.getQuery(telephone,"Customers","Telephone"),
                getFacade(),
                this::getEntityByResultSet);
    }

    @Override
    public List<Customer> getCustomersByAddressId(int addressID) {
        return QueryHelper.executeQuery(
                QueryHelper.getQuery(addressID,"Customers","AddressID"),
                getFacade(),
                this::getEntityByResultSet);
    }

    @Override
    public void updateCustomer(Customer customer) {
        if(customer==null){
            throw new CustomerException("update customer isn't possible by null");
        }
        getFacade().executeUpdate(
                "UPDATE Customers SET " +
                        "FirstName='"+customer.getFirstName()+"' ," +
                        "LastName='"+customer.getLastName()+"' ," +
                        "Email='"+customer.getEmail()+"' ," +
                        "Telephone='"+customer.getTelephone()+"' ," +
                        "AddressID="+customer.getAddressID()+" " +
                        "WHERE CustomerID="+customer.getCustomerID()+" ;"
        );
    }

    @Override
    public void insertCustomer(Customer customer) {
        if(customer==null){
            throw new CustomerException("insert customer isn't possible by null");
        }
        getFacade().executeUpdate(
                "INSERT INTO Customers(FirstName,LastName,Email,Telephone,AddressID) " +
                        "VALUES(" +
                        "'"+customer.getFirstName()+"'," +
                        "'"+customer.getLastName()+"'," +
                        "'"+customer.getEmail()+"'," +
                        "'"+customer.getTelephone()+"'," +
                        ""+customer.getAddressID()+");"
        );
    }

    @Override
    public void deleteCustomerById(int id) {
        if(id<=0){
            throw new CustomerException("delete customer isn't possible by zero or negative number");
        }
        getFacade().executeUpdate("DELETE FROM Customers WHERE CustomerID="+id+";");
    }

    @Override
    public Customer getEntityByResultSet(ResultSet set) {
        Customer customer = new Customer();
        try {
            customer.setCustomerID(set.getInt("CustomerID"));
            customer.setAddressID(set.getInt("AddressID"));
            customer.setFirstName(set.getString("FirstName"));
            customer.setLastName(set.getString("LastName"));
            customer.setTelephone(set.getString("Telephone"));
            customer.setEmail(set.getString("Email"));
            return customer;
        }catch (SQLException sqle){
            throw new CustomerException(sqle.getMessage());
        }
    }
}
