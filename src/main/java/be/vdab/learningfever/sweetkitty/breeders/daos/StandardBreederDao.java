package be.vdab.learningfever.sweetkitty.breeders.daos;

import be.vdab.learningfever.sweetkitty.breeders.Breeder;
import be.vdab.learningfever.sweetkitty.breeders.BreederDao;
import be.vdab.learningfever.sweetkitty.breeders.exceptions.BreederException;
import be.vdab.learningfever.sweetkitty.facade.JdbcFacade;
import be.vdab.learningfever.sweetkitty.util.EntityGetter;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import static be.vdab.learningfever.sweetkitty.util.QueryHelper.*;

public class StandardBreederDao implements BreederDao, EntityGetter<Breeder> {
    private JdbcFacade facade;

    public StandardBreederDao() {
        this(new JdbcFacade());
    }

    public StandardBreederDao(JdbcFacade facade) {
        this.setFacade(facade);
    }

    public JdbcFacade getFacade() {
        return facade;
    }

    public void setFacade(JdbcFacade facade) {
        this.facade = facade;
    }

    @Override
    public Breeder getBreederById(int breederId) {
        try(
                ResultSet set = getFacade().executeQuery(
                        getQuery(breederId,"Breeders","breederID")
                )
        ){
            if(set.next()){
                return getEntityByResultSet(set);
            }

        }catch (SQLException sqle){
            throw new BreederException(sqle.getMessage());
        }
        return null;
    }

    @Override
    public List<Breeder> getBreedersByCustomerId(int customerId) {
        return executeQuery(
                getQuery(customerId,"Breeders","CustomerID"),
                getFacade(),
                this::getEntityByResultSet
        );
    }

    @Override
    public List<Breeder> getBreedersByAddressId(int addressId) {
        return executeQuery(
                getQuery(addressId,"Breeders","AddressID"),
                getFacade(),
                this::getEntityByResultSet
        );
    }

    @Override
    public List<Breeder> getBreedersByCompanyName(String companyName) {
        return executeQuery(
                getQuery(companyName,"Breeders","CompanyName"),
                getFacade(),
                this::getEntityByResultSet
        );
    }

    @Override
    public void updateBreeders(Breeder breeder) {
        if(breeder == null){
            throw new BreederException("update breeder is not possible by null");
        }
        getFacade().executeUpdate(
                "UPDATE Breeders SET " +
                        "CompanyName = '"+breeder.getCompanyName()+"' ," +
                        "AddressID = "+breeder.getAddressId()+" ," +
                        "CustomerID = "+breeder.getCustomerId()+" " +
                        "WHERE BreederID = "+breeder.getBreederId()+" ;"
        );
    }

    @Override
    public void insertBreeder(Breeder breeder) {
        if(breeder == null){
            throw new BreederException("insert breeder is not possible by null");
        }
        getFacade().executeQuery(
                "INSERT INTO Breeders(CompanyName,CustomerID,AddressID) " +
                        "VALUES(" +
                        "'"+breeder.getCompanyName()+"'," +
                        ""+breeder.getCustomerId()+" ," +
                        ""+breeder.getAddressId()+");"
        );

    }

    @Override
    public void deleteBreeder(int breederId) {
        if(breederId <= 0){
            throw new BreederException("delete breeder is not possible by zero or negative number");
        }
        getFacade().executeQuery(
                "DELETE FROM Breeders WHERE BreederID = "+breederId+" ;"
        );
    }

    @Override
    public Breeder getEntityByResultSet(ResultSet set) {
        Breeder breeder = new Breeder();
        try{
        breeder.setBreederId(set.getInt("BreederID"));
        breeder.setAddressId(set.getInt("AddressID"));
        breeder.setCustomerId(set.getInt("CustomerID"));
        breeder.setCompanyName(set.getString("CompanyName"));
        return breeder;
        }catch(SQLException sqle){
            throw new BreederException(sqle.getMessage());
        }
    }
}
