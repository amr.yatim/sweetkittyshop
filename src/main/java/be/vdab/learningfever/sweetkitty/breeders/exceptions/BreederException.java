package be.vdab.learningfever.sweetkitty.breeders.exceptions;

public class BreederException extends RuntimeException {
    public BreederException() {
    }

    public BreederException(String message) {
        super(message);
    }

    public BreederException(String message, Throwable cause) {
        super(message, cause);
    }

    public BreederException(Throwable cause) {
        super(cause);
    }

    public BreederException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
