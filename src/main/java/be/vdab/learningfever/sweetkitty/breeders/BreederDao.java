package be.vdab.learningfever.sweetkitty.breeders;

import java.util.List;

public interface BreederDao {
    Breeder getBreederById(int breederId);
    List<Breeder> getBreedersByCustomerId(int customerId);
    List<Breeder> getBreedersByAddressId(int addressId);
    List<Breeder> getBreedersByCompanyName(String companyName);
    void updateBreeders(Breeder breeder);
    void insertBreeder(Breeder breeder);
    void deleteBreeder(int breederId);
}
