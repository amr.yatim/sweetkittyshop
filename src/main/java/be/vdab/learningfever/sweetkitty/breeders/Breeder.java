package be.vdab.learningfever.sweetkitty.breeders;

import be.vdab.learningfever.sweetkitty.exceptions.InvalidLengthStringException;
import be.vdab.learningfever.sweetkitty.exceptions.InvalidNumberException;
import be.vdab.learningfever.sweetkitty.exceptions.NullValueException;

import java.util.Objects;

public class Breeder {
    private int breederId;
    private int addressId;
    private String companyName;
    private int customerId;

    public Breeder() {
    }

    public int getBreederId() {
        return breederId;
    }

    public void setBreederId(int breederId) {
        if(breederId < 0){
            throw new InvalidNumberException("Breeder ID can't be Zero");
        }
        this.breederId = breederId;
    }

    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        if(addressId <= 0){
            throw new InvalidNumberException("Address ID can't be Zero or Negative");
        }
        this.addressId = addressId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        if(companyName == null){
            throw new NullValueException("Company name can't be NULL");
        }
        if(companyName.length() < 2 || companyName.length() > 100){
            throw new InvalidLengthStringException(
                    "Company Name must contain more than 2 characters and les than 100 characters");
        }
        this.companyName = companyName;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        if(customerId <= 0){
            throw new InvalidNumberException("Customer ID can't be Zero or Negative");
        }
        this.customerId = customerId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Breeder breeder = (Breeder) o;
        return getBreederId() == breeder.getBreederId() &&
                getAddressId() == breeder.getAddressId() &&
                getCustomerId() == breeder.getCustomerId() &&
                getCompanyName().equalsIgnoreCase(breeder.getCompanyName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(breederId, addressId, companyName, customerId);
    }

    @Override
    public String toString() {
        return "Breeder{" +
                "breederId=" + breederId +
                ", addressId=" + addressId +
                ", companyName='" + companyName + '\'' +
                ", customerId=" + customerId +
                '}';
    }
}
