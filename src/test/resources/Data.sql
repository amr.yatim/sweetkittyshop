TRUNCATE TABLE Breeders RESTART IDENTITY;
TRUNCATE TABLE Orders RESTART IDENTITY;
TRUNCATE TABLE Customers RESTART IDENTITY;
TRUNCATE TABLE Addresses RESTART IDENTITY;
TRUNCATE TABLE Varieties RESTART IDENTITY;

insert into Addresses (Street, HouseNumber, Bus, ZipCode, City, Country)
values
('Fraiselaan', 22, NULL, NULL, 'Oostende', 'België'),
('Brugse Steenweg', 183, 'B', '9990' , 'Maldegem', 'België'),
('Fraiselaan', 1, 'A', '9000', 'Gent', 'België'),
('Vleeshuisstraat', 174, NULL, '9000', 'Gent', 'België');

insert into Customers (FirstName, LastName, Email, Telephone, AddressID)
values
('Karolien', 'De Smet', 'karolien.de.smet@gmail.com', '0032473645399', 3),
('Freya', 'Lievens', 'freya.poessie@hotmail.com', '0032492332233', 2),
('Karel', 'de Grote', 'karelvagrote@hotmail.com', '0032487756473', 3),
('Suzzy', 'Lalijo', 'suzzy.wuzzy@mee.com', '0032499934843', 4),
('Amr','Yatim','omaryateem@gmail.com','0032489260340',1);

insert into Orders (OrderDate, CustomerID)
values
('2019-09-02', 7);

insert into Varieties (Name, Description, Price)
values
('Heilige Birmaan', 'Heilige Birmaan kat', 400.0),
('Britse Korthaar', 'Britse Korthaar kat', 250.0),
('Europese Korthaar', 'Europese Korthaar kat', 50.0),
('Naakt', 'Naakt kat', 375.0),
('Perzische', 'Perzische kat', 830.0);

insert into Breeders (CompanyName, CustomerID, AddressID)
values
('Black Mystic Legend', 8, 8),
('Sequoia hoeve', 9, 9),
('Ankeshof', 10, 10),
('Lieve Poezies', 11, 11),
('Katia s Palazo', 12, 12),
('Kitty wizzy', 13, 13),
('Groot Kathof', 14, 14);