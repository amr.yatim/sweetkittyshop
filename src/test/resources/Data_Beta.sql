truncate table Customers;
truncate table Breeders;
truncate table Varieties;
truncate table variety_Breeder_Couple;
truncate table SoldItem;
truncate table Orders;


insert into Customers (CustomerID,FirstName, LastName, Email, Telephone, AddressID)
values
(1,'Karolien', 'De Smet', 'karolien.de.smet@gmail.com', '0032473645399', 1),
(2,'Freya', 'Lievens', 'freya.poessie@hotmail.com', '0032492332233', 2),
(3,'Karel', 'de Grote', 'karelvagrote@htomail.com', '0032487756473', 3),
(4,'Suzzy', 'Lalijo', 'suzzy.wuzzy@me.com', '0032499934843', 4),
(5,'Levi', 'Helders', 'levi@multimedi.be', '003241234563', 5),
(6,'Fleur', 'Potters', 'fleur@msn.be', '0032472323645', 6),
(7,'Kelly', 'Verlinde', 'kellieke@versegroenten.be', '0031610283736', 7),
(8,'Sonja', 'Verliefde', 'info@blackmystic.be', '0032475101044', 8),
(9,'Sandra', 'Gezelle', 'info@sequoiahoeve.nl', '0031688347273', 9),
(10,'Anke', 'Van Hoven', 'info@ankeshof.be', '0032484328432', 10),
(11,'Lieve', 'De Schat', 'lieve.poezies@gmail.com', '0032490101010', 11),
(12,'Katia', 'Ligatino', 'info@katias.palazo.be', '0032483618361', 12),
(13,'Wiese', 'Berlind', 'wizzy.info@kitties.be', '0032491231239', 13),
(14,'Catharina', 'Bruyntjes', 'info@groot-kathof.be', '0032472793542', 14);

insert into Orders (OrderID,OrderDate, CustomerID)
values
(1,'2019-09-02', 7);

insert into Varieties (VarietyID,Name, Description, Price)
values
(1,'Heilige Birmaan', 'Heilige Birmaan kat', 400.0),
(2,'Britse Korthaar', 'Britse Korthaar kat', 250.0),
(3,'Europese Korthaar', 'Europese Korthaar kat', 50.0),
(4,'Naakt', 'Naakt kat', 375.0),
(5,'Perzische', 'Perzische kat', 830.0),
(6,'Maine Coon', 'Gigantische kat', 675.0),
(7,'Noorse Bos', 'Wilde kat', 345.0),
(8,'Siamees', 'Schele kat', 660.0),
(9,'Bengaal', 'Is het een Kat', 900.0);

insert into Breeders (BreederID,CompanyName, CustomerID, AddressID)
values
(1,'Black Mystic Legend', 8, 8),
(2,'Sequoia hoeve', 9, 9),
(3,'Ankeshof', 10, 10),
(4,'Lieve Poezies', 11, 11),
(5,'Katia s Palazo', 12, 12),
(6,'Kitty wizzy', 13, 13),
(7,'Groot Kathof', 14, 14);

insert into SoldItem (ItemID,OrderID, VarietyID, BreederID, Amount, SellingPrice)
values
(1,1, 2, 2, 1, 250),
(2,1, 1, 1, 1, 500);

insert into Variety_Breeder_Couple (CoupleID,BreederID, VarietyID, Amount)
values
(1,1, 1, 3),
(2,2, 2, 5),
(3,2, 3, 3),
(4,3, 4, 5),
(5,4, 5, 2),
(6,4, 6, 5),
(7,5, 7, 7),
(8,5, 3, 7),
(9,6, 8, 5),
(10,6, 4, 8),
(11,7, 1, 2),
(12,7, 4, 1),
(13,7, 5, 6),
(14,7, 8, 10),
(15,7, 7, 17),
(16,7, 3, 2),
(17,7, 9, 5),
(18,7, 2, 3),
(19,7, 6, 5);
