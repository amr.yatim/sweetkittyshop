TRUNCATE TABLE Orders RESTART IDENTITY;

insert into Orders (OrderDate, CustomerID)
values
('2019-09-02', 5),
('2019-09-02', 1),
('2020-01-02', 5),
('2020-02-29', 3);