TRUNCATE TABLE Customers RESTART IDENTITY;

insert into Customers (FirstName, LastName, Email, Telephone, AddressID)
values
('Suzzy', 'de Grote', 'suzzy.wuzzy@mee.com', '0032473645399', 3),
('Freya', 'Lievens', 'freya.poessie@hotmail.com', '0032492332233', 2),
('Karel', 'de Grote', 'karelvagrote@hotmail.com', '0032492332233', 3),
('Suzzy', 'Lalijo', 'suzzy.wuzzy@mee.com', '0032499934843', 4),
('Amr','Yatim','amr.yatim@gmail.com','0032489260340',1);