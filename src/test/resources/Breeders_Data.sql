TRUNCATE TABLE Breeders RESTART IDENTITY;

insert into Breeders (CompanyName, CustomerID, AddressID)
values
('Black Mystic Legend', 8, 8),
('Kitty wizzy', 9, 9),
('Ankeshof', 10, 10),
('Lieve Poezies', 11, 11),
('Katia s Palazo', 10, 12),
('Kitty wizzy', 13, 13),
('Groot Kathof', 14, 12);