CREATE TABLE Customers (
CustomerID INT NOT NULL IDENTITY,
FirstName VARCHAR(45) NOT NULL,
LastName VARCHAR(45) NOT NULL,
Email VARCHAR(255) NOT NULL,
Telephone VARCHAR(16) NOT NULL,
AddressID INT NOT NULL,
PRIMARY KEY (CustomerID),
Foreign key (AddressID) references Addresses(AddressID));

CREATE TABLE  Orders (
OrderID INT NOT NULL IDENTITY,
OrderDate DATETIME NOT NULL,
CustomerID INT NOT NULL,
PRIMARY KEY (OrderID),
Foreign key (CustomerID) references Customers(CustomerID));

CREATE TABLE Varieties (
VarietyID INT NOT NULL IDENTITY,
Name VARCHAR(45) NOT NULL,
Description VARCHAR(255) NOT NULL,
Price DOUBLE NOT NULL,
PRIMARY KEY (VarietyID));

CREATE TABLE  Breeders (
BreederID INT NOT NULL IDENTITY,
AddressID INT NOT NULL,
CompanyName VARCHAR(100) NOT NULL,
CustomerID INT NOT NULL,
PRIMARY KEY (BreederID),
Foreign key (AddressID) references Addresses(AddressID),
Foreign key (CustomerID) references Customers(CustomerID));

CREATE TABLE  SoldItem (
ItemID INT NOT NULL IDENTITY,
OrderID INT NOT NULL,
VarietyID INT NOT NULL,
BreederID INT NOT NULL,
Amount INT NOT NULL,
SellingPrice DOUBLE NOT NULL,
PRIMARY KEY (ItemID),
Foreign key (OrderID) references Orders(OrderID),
Foreign key (BreederID) references Breeders(BreederID),
Foreign key (VarietyID) references Varieties(VarietyID));

CREATE  TABLE  Variety_Breeder_Couple (
CoupleID INT NOT NULL IDENTITY,
BreederID INT NOT NULL,
VarietyID INT NOT NULL,
Amount INT NOT NULL,
PRIMARY KEY (CoupleID),
Foreign key (BreederID) references Breeders(BreederID),
Foreign key (VarietyID) references Varieties(VarietyID));