TRUNCATE TABLE Addresses RESTART IDENTITY;

insert into Addresses (Street, HouseNumber, Bus, ZipCode, City, Country)
values
('Fraiselaan', 22, NULL, NULL, 'Oostende', 'België'),
('Brugse Steenweg', 183, 'B', '9990' , 'Maldegem', 'België'),
('Fraiselaan', 1, 'A', '9000', 'Gent', 'België'),
('Vleeshuisstraat', 174, NULL, '9000', 'Gent', 'België');