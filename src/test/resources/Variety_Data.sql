TRUNCATE TABLE Varieties RESTART IDENTITY;

insert into Varieties (Name, Description, Price)
values
('Heilige Birmaan', 'Heilige Birmaan kat', 400.0),
('Britse Korthaar', 'Britse Korthaar kat', 250.0),
('Europese Korthaar', 'Europese Korthaar kat', 50.0),
('Naakt', 'Naakt kat', 375.0),
('Perzische', 'Perzische kat', 830.0),
('Naakt', 'fake kat', 50.0);