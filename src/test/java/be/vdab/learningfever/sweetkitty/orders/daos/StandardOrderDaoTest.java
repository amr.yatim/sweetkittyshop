package be.vdab.learningfever.sweetkitty.orders.daos;

import be.vdab.learningfever.sweetkitty.cutomers.Customer;
import be.vdab.learningfever.sweetkitty.facade.JdbcFacade;
import be.vdab.learningfever.sweetkitty.orders.Order;
import be.vdab.learningfever.sweetkitty.orders.exceptions.OrderException;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

class StandardOrderDaoTest {
    private static final String MEM_PATH="src/test/resources/mainInfo.properties" ;
    StandardOrderDao sod;
    List<Order> listOrders;

    @BeforeEach
    void init(){
        sod = new StandardOrderDao(new JdbcFacade(MEM_PATH));
        listOrders = new ArrayList<>();
        Order order = new Order();
        order.setOrderId(1);
        order.setOrderDate("2019-09-02");
        order.setCustomerId(5);
        listOrders.add(order);

        order = new Order();
        order.setOrderId(2);
        order.setOrderDate("2019-09-02");
        order.setCustomerId(1);
        listOrders.add(order);

        order = new Order();
        order.setOrderId(3);
        order.setOrderDate("2020-01-02");
        order.setCustomerId(5);
        listOrders.add(order);

        order = new Order();
        order.setOrderId(4);
        order.setOrderDate("2020-02-29");
        order.setCustomerId(3);
        listOrders.add(order);


        try(
                BufferedReader schemaReader = new BufferedReader(new FileReader("src/test/resources/Orders_Schema.sql"));
                BufferedReader dataReader = new BufferedReader(new FileReader("src/test/resources/Orders_Data.sql"))
        ){
            ScriptRunner runner = new ScriptRunner(sod.getFacade().getConnect());
            runner.runScript(schemaReader);
            runner.runScript(dataReader);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }

    }

    @Test
    @DisplayName("Get Order By orderId")
    void testGetOrderById() {
        checkGetOrderByOrderIdExists();
        checkGetOrderByOrderIdNotExists();
    }

    private void checkGetOrderByOrderIdExists(){
        assertEquals(listOrders.get(0),
                sod.getOrderById(listOrders.get(0).getOrderId()),
                "Get order by OrderId doesn't work perfectly ");
    }
    private void checkGetOrderByOrderIdNotExists(){
        assertNull(sod.getOrderById(6),
                "Get Order by orderId not exists doesn't work perfectly");
    }

    @Test
    @DisplayName("test Get Order By CustomerId One Result")
    void testGetOrderByCustomerIdOneResult() {
        assertEquals(listOrders.get(1),
                sod.getOrdersByCustomerID(listOrders.get(1).getCustomerId()).get(0),
                "Get Order By CustomerId One Result doesn't work perfectly");
    }

    @Test
    @DisplayName("Get Orders By Customer Id Multi Result")
    void testGetOrderByCustomerIdMultiResult() {
        Order[] orders = {listOrders.get(0),listOrders.get(2)};
        assertArrayEquals(orders,
                sod.getOrdersByCustomerID(listOrders.get(0).getCustomerId()).toArray(Order[]::new),
                "Orders By Customer Id Multi Result don't work perfectly");
    }

    @Test
    @DisplayName("Get Order By CustomerId doesn't exists")
    void testGetOrderByCustomerIdNoResult() {
        assertEquals(new ArrayList<Order>(),
                sod.getOrdersByCustomerID(4),
                "Get Order By CustomerId doesn't exists, doesn't work perfectly");
    }

    @Test
    @DisplayName("test Get Order By OrderDate(String) One Result")
    void testGetOrderByStringOrderDateOneResult() {
        assertEquals(listOrders.get(3),
                sod.getOrdersByDate(listOrders.get(3).getOrderDateAsString()).get(0),
                "Get Order By OrderDate(String) One Result doesn't work perfectly");
    }

    @Test
    @DisplayName("Get Orders By OrderDate(String) Multi Result")
    void testGetOrderByStringOrderDateMultiResult() {
        Order[] orders = {listOrders.get(0),listOrders.get(1)};
        assertArrayEquals(orders,
                sod.getOrdersByDate(listOrders.get(0).getOrderDateAsString()).toArray(Order[]::new),
                "Orders By OrderDate(String) Multi Result don't work perfectly");
    }

    @Test
    @DisplayName("Get Order By OrderDate(String) doesn't exists")
    void testGetOrderByStringOrderDateNoResult() {
        assertEquals(new ArrayList<Order>(),
                sod.getOrdersByDate("1990-01-01"),
                "Get Order By OrderDate(String) doesn't exists, doesn't work perfectly");
    }

    @Test
    @DisplayName("test Get Order By OrderDate(LocalDate)One Result")
    void testGetOrderByLocalDateOrderDateOneResult() {
        assertEquals(listOrders.get(3),
                sod.getOrdersByDate(listOrders.get(3).getOrderDate()).get(0),
                "Get Order By OrderDate(LocalDate) One Result doesn't work perfectly");
    }

    @Test
    @DisplayName("Get Orders By OrderDate(LocalDate) Multi Result")
    void testGetOrderByLocalDateOrderDateMultiResult() {
        Order[] orders = {listOrders.get(0),listOrders.get(1)};
        assertArrayEquals(orders,
                sod.getOrdersByDate(listOrders.get(0).getOrderDate()).toArray(Order[]::new),
                "Orders By OrderDate(LocalDate) Multi Result don't work perfectly");
    }

    @Test
    @DisplayName("Get Order By OrderDate(LocalDate) doesn't exists")
    void testGetOrderByLocalDateOrderDateNoResult() {
        assertEquals(new ArrayList<Order>(),
                sod.getOrdersByDate(LocalDate.parse(
                        "1990-01-01",
                        DateTimeFormatter.ofPattern(Order.ORDER_DATE_TIME_FORMAT))),
                "Get Order By OrderDate(LocalDate) doesn't exists, doesn't work perfectly");
    }


    @Test
    @DisplayName("test Update Orders ")
    void updateOrder() {
        Order order = listOrders.get(3);
        order.setOrderDate("1989-04-08");
        order.setCustomerId(7);
        sod.updateOrder(order);
        assertEquals(
                order,
                sod.getOrderById(order.getOrderId()),
                "Update orders doesn't work perfectly");
    }

    @Test
    @DisplayName("test Update Orders(null) ")
    void updateOrderNull() {
        assertThrows(
                OrderException.class,
                ()->sod.updateOrder(null),
                "update order isn't possible by null"
        );
    }

    @Test
    @DisplayName("test Insert Order")
    void insertOrder() {
        Order order = new Order();
        order.setOrderId(5);
        order.setOrderDate("1989-04-08");
        order.setCustomerId(7);
        sod.insertOrder(order);
        assertEquals(
                order,
                sod.getOrderById(5),
                "Insert order doesn't work perfectly");
    }

    @Test
    @DisplayName("test Insert Orders(null) ")
    void insertOrderNull() {
        assertThrows(
                OrderException.class,
                ()->sod.insertOrder(null),
                "insert order isn't possible by null"
        );
    }

    @Test
    @DisplayName("test Delete Order")
    void deleteOrder() {
        sod.deleteOrder(listOrders.get(0).getOrderId());
        assertNull(sod.getOrderById(listOrders.get(0).getOrderId()),
                "Delete order doesn't work perfectly");
    }

    @Test
    @DisplayName("test delete Orders(Zero) ")
    void deleteOrderZero() {
        assertThrows(
                OrderException.class,
                ()->sod.deleteOrder(0),
                "delete order isn't possible by zero or negative number"
        );
    }

    @AfterEach
    void destroy(){
        try {
            sod.getFacade().close();
        }catch (Exception e){
            e.printStackTrace();
        }
        listOrders.clear();
        listOrders = null;
        sod = null;
    }
}