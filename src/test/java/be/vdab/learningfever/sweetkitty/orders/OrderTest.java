package be.vdab.learningfever.sweetkitty.orders;

import be.vdab.learningfever.sweetkitty.exceptions.InvalidDateException;
import be.vdab.learningfever.sweetkitty.exceptions.InvalidNumberException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.condition.DisabledIf;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.*;

class OrderTest {
    private Order order;

    @BeforeEach
    void init(){
        order = new Order();
    }

    @Test
    @DisplayName("Test order id setter and getter")
    void testOrderId() {
        checkOrderIdNegative();
        checkOrderIdGetterAndSetter();
    }

    private void checkOrderIdNegative(){
        assertThrows(
                InvalidNumberException.class,
                ()->order.setOrderId(-1),
                "Order Id can't be Negative"
        );
    }
    private void checkOrderIdGetterAndSetter(){
        order.setOrderId(1);
        assertEquals(1,order.getOrderId(),
                "OrderId getter and setter don't work perfectly");
    }

    @Test
    @DisplayName("test order date and time setter and getter By LocalDate")
    void testOrderDateTimeByLocaleDate() {
        checkOrderDateByLocalDate();
        checkOrderInvalidByLocaleDate();
    }

    private void checkOrderDateByLocalDate(){
        LocalDate ld = LocalDate.now();
        order.setOrderDate(ld);
        assertEquals(ld,order.getOrderDate(),"" +
                "OrderDate By LocalDate doesn't work perfectly");
    }
    private void checkOrderInvalidByLocaleDate(){
        assertThrows(InvalidDateException.class,
                ()->order.setOrderDate(LocalDate.now().plusDays(1)),
                "OrderDate can't be after the current date");
    }
    @Test
    @DisplayName("test order date and time setter and getter By String")
    void testOrderDateTimeByString() {
        checkOrderDateByString();
        checkOrderInvalidByString();
    }

    private void checkOrderDateByString(){
        LocalDate ld = LocalDate.parse("2020-01-01",
                DateTimeFormatter.ofPattern(Order.ORDER_DATE_TIME_FORMAT));
        order.setOrderDate("2020-01-01");
        assertEquals(ld,order.getOrderDate(),"" +
                "OrderDate By String doesn't work perfectly");
    }
    private void checkOrderInvalidByString(){
        assertThrows(InvalidDateException.class,
                ()->order.setOrderDate(
                        LocalDate.now().plusDays(1).format(
                                DateTimeFormatter.ofPattern(Order.ORDER_DATE_TIME_FORMAT)
                        )
                ),
                "OrderDate can't be after the current date");
    }

    @Test
    @DisplayName("test customer id setter and getter")
    void testCustomerId() {
        checkCustomerIdZero();
        checkCustomerIdNegative();
        checkCustomerIdGetterAndSetter();
    }

    private void checkCustomerIdZero(){
        assertThrows(
                InvalidNumberException.class,
                ()->order.setCustomerId(0),
                "Customer Id can't be Zero"
        );
    }
    private void checkCustomerIdNegative(){
        assertThrows(
                InvalidNumberException.class,
                ()->order.setCustomerId(-1),
                "Customer Id can't be Negative"
        );
    }
    private void checkCustomerIdGetterAndSetter(){
        order.setCustomerId(1);
        assertEquals(1,order.getCustomerId(),
                "CustomerID getter and setter don't work perfectly");
    }

    @Test
    @DisplayName("test Get orderDate as string")
    void testGetOrderDateAsString(){
        LocalDate ld = LocalDate.now();
        String acceptedDate = ld.format(
                DateTimeFormatter.ofPattern(Order.ORDER_DATE_TIME_FORMAT)
        );
        order.setOrderDate(ld);
        assertEquals(acceptedDate,
                order.getOrderDateAsString(),
                "Get OrderDate as String doesn't perfectly");
    }

    @AfterEach
    void destroy(){
        order = null;
    }
}