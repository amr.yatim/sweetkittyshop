package be.vdab.learningfever.sweetkitty.cutomers.daos;

import be.vdab.learningfever.sweetkitty.cutomers.Customer;
import be.vdab.learningfever.sweetkitty.cutomers.exceptions.CustomerException;
import be.vdab.learningfever.sweetkitty.facade.JdbcFacade;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StandardCustomerDaoTest {
    private static final String MEM_PATH="src/test/resources/mainInfo.properties" ;
    private StandardCustomerDao scd;
    private List<Customer> listCustomers;

    @BeforeEach
    void init(){
        listCustomers = new ArrayList<>();
        //listCustomers.sort((c1,c2)->c2.getCustomerID()-c1.getCustomerID());
        scd = new StandardCustomerDao(new JdbcFacade(MEM_PATH));
        Customer customer = new Customer();

        customer.setCustomerID(1);
        customer.setFirstName("Suzzy");
        customer.setLastName("de Grote");
        customer.setEmail("suzzy.wuzzy@mee.com");
        customer.setTelephone("0032473645399");
        customer.setAddressID(3);
        listCustomers.add(customer);

        customer = new Customer();
        customer.setCustomerID(2);
        customer.setFirstName("Freya");
        customer.setLastName("Lievens");
        customer.setEmail("freya.poessie@hotmail.com");
        customer.setTelephone("0032492332233");
        customer.setAddressID(2);
        listCustomers.add(customer);

        customer = new Customer();
        customer.setCustomerID(3);
        customer.setFirstName("Karel");
        customer.setLastName("de Grote");
        customer.setEmail("karelvagrote@hotmail.com");
        customer.setTelephone("0032492332233");
        customer.setAddressID(3);
        listCustomers.add(customer);

        customer = new Customer();
        customer.setCustomerID(4);
        customer.setFirstName("Suzzy");
        customer.setLastName("Lalijo");
        customer.setEmail("suzzy.wuzzy@mee.com");
        customer.setTelephone("0032499934843");
        customer.setAddressID(4);
        listCustomers.add(customer);

        customer = new Customer();
        customer.setCustomerID(5);
        customer.setFirstName("Amr");
        customer.setLastName("Yatim");
        customer.setEmail("amr.yatim@gmail.com");
        customer.setTelephone("0032489260340");
        customer.setAddressID(1);
        listCustomers.add(customer);

        try(
                BufferedReader schemaReader = new BufferedReader(new FileReader("src/test/resources/Customer_Schema.sql"));
                BufferedReader dataReader = new BufferedReader(new FileReader("src/test/resources/Customer_Data.sql"))
        ){
            ScriptRunner runner = new ScriptRunner(scd.getFacade().getConnect());
            runner.runScript(schemaReader);
            runner.runScript(dataReader);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }

    @Test
    @DisplayName("Test Get Customer By id exists")
    void testCustomerByIdExists() {
        assertEquals(listCustomers.get(0),
                scd.getCustomerById(listCustomers.get(0).getCustomerID()),
                "Get Customer By id doesn't work perfectly");
    }

    @Test
    @DisplayName("Test Get Customer By id Not exists")
    void testCustomerByIdNotExists() {
        assertNull(scd.getCustomerById(7),
                "Get Customer by id that doesn't exists, doesn't work perfectly");
    }

    @Test
    @DisplayName("Get Customer By First Name One Result")
    void testCustomersByFirstNameOneResult() {
        assertTrue(
                scd.getCustomersByFirstName("Amr").size()==1,
                "Get one result Customer By First Name doesn't work perfectly");
    }

    @Test
    @DisplayName("Get Customer By First Name Multi Result")
    void testCustomersByFirstNameMultiResult() {
        Customer[] list ={listCustomers.get(0),listCustomers.get(3)};
        assertArrayEquals(list,
                scd.getCustomersByFirstName("Suzzy").toArray(Customer[]::new),
                "Get Multi result Customer By First Name doesn't work perfectly");
    }

    @Test
    @DisplayName("Get Customer By First Name doesn't exists")
    void testCustomersByFirstNameNoResult() {
        assertEquals(new ArrayList<Customer>(),
                scd.getCustomersByFirstName("omar"),
                "Get Customer By First Name doesn't exists, doesn't work perfectly");
    }

    @Test
    @DisplayName("Get Customer By Last Name One Result")
    void testCustomersByFLastNameOneResult() {
        assertTrue(
                scd.getCustomersByLastName("yatim").size()==1,
                "Get one result Customer By Last Name doesn't work perfectly");
    }

    @Test
    @DisplayName("Get Customer By Last Name Multi Result")
    void testCustomersByLastNameMultiResult() {
        Customer[] list ={listCustomers.get(0),listCustomers.get(2)};
        assertArrayEquals(list,
                scd.getCustomersByLastName("de Grote").toArray(Customer[]::new),
                "Get Multi result Customer By Last Name doesn't work perfectly");
    }

    @Test
    @DisplayName("Get Customer By Last Name doesn't exists")
    void testCustomersByLastNameNoResult() {
        assertEquals(new ArrayList<Customer>(),
                scd.getCustomersByLastName("yateem"),
                "Get Customer By Last Name doesn't exists, doesn't work perfectly");
    }

    @Test
    @DisplayName("Get Customer By Email One Result")
    void testCustomersByEmailOneResult() {
        assertTrue(
                scd.getCustomersByEmail("amr.yatim@gmail.com").size()==1,
                "Get one result Customer By Email doesn't work perfectly"
        );
    }

    @Test
    @DisplayName("Get Customer By Email Multi Result")
    void testCustomersByEmailMultiResult() {
        Customer[] list ={listCustomers.get(0),listCustomers.get(3)};
        assertArrayEquals(list,
                scd.getCustomersByEmail("suzzy.wuzzy@mee.com").toArray(Customer[]::new),
                "Get Multi result Customer By Email doesn't work perfectly");
    }

    @Test
    @DisplayName("Get Customer By Email doesn't exists")
    void testCustomersByEmailNoResult() {
        assertEquals(new ArrayList<Customer>(),
                scd.getCustomersByEmail("omaryateem@gmail.com"),
                "Get Customer By Email doesn't exists, doesn't work perfectly");
    }

    @Test
    @DisplayName("Get Customer By Telephone One Result")
    void testCustomersByTelephoneOneResult() {
        assertTrue(
                scd.getCustomersByTelephone("0032489260340").size()==1,
                "Get one result Customer By Email doesn't work perfectly");
    }

    @Test
    @DisplayName("Get Customer By Telephone Multi Result")
    void testCustomersByTelephoneMultiResult() {
        Customer[] list ={listCustomers.get(1),listCustomers.get(2)};
        assertArrayEquals(list,
                scd.getCustomersByTelephone("0032492332233").toArray(Customer[]::new),
                "Get Multi result Customer By Telephone doesn't work perfectly");
    }

    @Test
    @DisplayName("Get Customer By Telephone doesn't exists")
    void testCustomersByTelephoneNoResult() {
        assertEquals(new ArrayList<Customer>(),
                scd.getCustomersByTelephone("123456712345"),
                "Get Customer By Telephone doesn't exists, doesn't work perfectly");
    }

    @Test
    @DisplayName("Get Customer By AddressId One Result")
    void testCustomersByAddressIdOneResult() {
        assertTrue(
                scd.getCustomersByAddressId(1).size()==1,
                "Get one result Customer By AddressId doesn't work perfectly");
    }

    @Test
    @DisplayName("Get Customer By AddressId Multi Result")
    void testCustomersByAddressIdMultiResult() {
        Customer[] list ={listCustomers.get(0),listCustomers.get(2)};
        assertArrayEquals(list,
                scd.getCustomersByAddressId(3).toArray(Customer[]::new),
                "Get Multi result Customer By AddressId doesn't work perfectly");
    }

    @Test
    @DisplayName("Get Customer By AddressId doesn't exists")
    void testCustomersByAddressIDNoResult() {
        assertEquals(new ArrayList<Customer>(),
                scd.getCustomersByAddressId(10),
                "Get Customer By AddressId doesn't exists, doesn't work perfectly");
    }

    @Test
    @DisplayName("Update Customer")
    void testUpdateCustomer() {
        Customer customer = listCustomers.get(4);
        customer.setFirstName("Omar");
        customer.setLastName("Yateem");
        customer.setEmail("omaryateem@hotmail.com");
        scd.updateCustomer(customer);
        assertEquals(customer,
                scd.getCustomerById(customer.getCustomerID()),
                "Update customer doesn't work perfectly");
    }

    @Test
    @DisplayName("Update Customer(null)")
    void testUpdateCustomerNull() {
        assertThrows(
                CustomerException.class,
                ()->scd.updateCustomer(null),
                "update customer isn't be possible by null"
        );
    }

    @Test
    @DisplayName("Insert Customer")
    void testInsertCustomer() {
        Customer customer = new Customer();
        customer.setCustomerID(6);
        customer.setFirstName("Omar");
        customer.setLastName("Yateem");
        customer.setEmail("omaryateem@hotmail.com");
        customer.setTelephone("0032489260340");
        customer.setAddressID(1);
        scd.insertCustomer(customer);
        assertEquals(customer,
                scd.getCustomerById(customer.getCustomerID()),
                "Insert customer doesn't work perfectly");
    }

    @Test
    @DisplayName("Insert Customer(null)")
    void testInsertCustomerNull() {
        assertThrows(
                CustomerException.class,
                ()->scd.insertCustomer(null),
                "insert customer isn't be possible by null"
        );
    }

    @Test
    @DisplayName("Delete Customer")
    void testDeleteCustomerById() {
        scd.deleteCustomerById(listCustomers.get(4).getCustomerID());
        assertNull(scd.getCustomerById(listCustomers.get(4).getCustomerID()),
                "Delete Customer doesn't work perfectly");
    }

    @Test
    @DisplayName("Delete Customer(Zero)")
    void testDeleteCustomerZero() {
        assertThrows(
                CustomerException.class,
                ()->scd.deleteCustomerById(0),
                "update customer isn't be possible by null"
        );
    }

    @AfterEach
    void destroy(){
        try {
            scd.getFacade().close();
        }catch (Exception e){
            e.printStackTrace();
        }

        scd = null;
    }
}