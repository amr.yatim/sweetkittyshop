package be.vdab.learningfever.sweetkitty.cutomers;

import be.vdab.learningfever.sweetkitty.cutomers.exceptions.InvalidEmailException;
import be.vdab.learningfever.sweetkitty.exceptions.InvalidCharacters;
import be.vdab.learningfever.sweetkitty.exceptions.InvalidLengthStringException;
import be.vdab.learningfever.sweetkitty.exceptions.InvalidNumberException;
import be.vdab.learningfever.sweetkitty.exceptions.NullValueException;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class CustomerTest {

    Customer customer;

    @BeforeEach
    void init(){
        customer = new Customer(
                "Amr",
                "Yatim",
                "omaryateem@gmail.com",
                "0489260340",
                2
        );
        customer.setCustomerID(5);
    }

    @Test
    @DisplayName("Customer ID Getter and Setter")
    void testCustomerID() {
        checkCustomerIdNegativeVale();
        checkCustomerIDGetterAndSetter();
    }
    private void checkCustomerIdNegativeVale(){
        assertThrows(InvalidNumberException.class,
                ()->customer.setCustomerID(-1),
                "The ID of the address can't be Negative");
    }
    private void checkCustomerIDGetterAndSetter(){
        customer.setCustomerID(1);
        assertEquals(1,
                customer.getCustomerID(),
                "The ID Customer getter and setter don't work perfectly");
    }

    @Test
    @DisplayName("First Name Getter and Setter")
    void testFirstName() {
        checkFistNameMinLength();
        checkFistNameMaxLength();
        checkFirstNameNotNull();
        checkFirstNameGetterAndSetter();
    }
    private void checkFistNameMinLength(){
        assertThrows(InvalidLengthStringException.class,
                ()->customer.setFirstName("AB"),
                "The first name must contains 3 characters at least");
    }
    private void checkFistNameMaxLength(){
        assertThrows(InvalidLengthStringException.class,
                ()->{
                    String s ="";
                    for (int i = 0; i < 46; i++) {
                        s += "s";
                    }
                    customer.setLastName(s);
                },
                "The first name must not contain more than 45 characters");
    }
    private void checkFirstNameNotNull(){
        assertThrows(NullValueException.class,
                ()->customer.setFirstName(null),
                "the first name can't be NULL");
    }
    private void checkFirstNameGetterAndSetter(){
        customer.setFirstName("Nieuwe Leven");
        assertEquals("Nieuwe Leven",
                customer.getFirstName(),
                "The getter and Setter for the first name don't work perfectly");
    }

    @Test
    @DisplayName("Last Name Getter and Setter")
    void testLastName() {
        checkLastNameMinLength();
        checkLastNameMaxLength();
        checkLastNameNotNull();
        checkLastNameGetterAndSetter();
    }
    private void checkLastNameMinLength(){
        assertThrows(InvalidLengthStringException.class,
                ()->customer.setFirstName("AB"),
                "The last name must contains 3 characters at least");
    }
    private void checkLastNameMaxLength(){
        assertThrows(InvalidLengthStringException.class,
                ()->{
                    String s ="";
                    for (int i = 0; i < 46; i++) {
                        s += "s";
                    }
                    customer.setLastName(s);
                },
                "The Last name must not contain more than 45 characters");
    }
    private void checkLastNameNotNull(){
        assertThrows(NullValueException.class,
                ()->customer.setFirstName(null),
                "the last name can't be NULL");
    }
    private void checkLastNameGetterAndSetter(){
        customer.setFirstName("Nieuwe Leven");
        assertEquals("Nieuwe Leven",
                customer.getFirstName(),
                "The getter and Setter for last name don't work perfectly");
    }

    @Test
    @DisplayName("E-mail Getter and Setter")
    void testEmail() {
        checkEmailContainIllegalBeforeAtCharacters();
        checkEmailContainIllegalAfterAtCharacters();
        checkEmailContainIllegalAfterAPointCharacters();
        checkEmailContainIllegalSetOfCharactersBeforeAt();
        checkEmailContainIllegalSetOfCharactersAfterAt();
        checkEmailContainIllegalSetOfCharactersAfterPoint();
        checkEmailWithoutAtCharacter();
        checkEmailWithoutPointCharacter();
        checkEmailValidForm();
        checkEmailNotNull();
        checkEmailGetterAndSetter();
    }
    private void checkEmailContainIllegalBeforeAtCharacters(){
        assertThrows(InvalidCharacters.class,
                ()->customer.setEmail("omar.yatim!@gmail.com"),
                "the e-mail contains illegal character before @");
    }
    private void checkEmailContainIllegalAfterAtCharacters(){
        assertThrows(InvalidCharacters.class,
                ()->customer.setEmail("omar.yatim@!gmail.com"),
                "the e-mail contains illegal character After @");
    }
    private void checkEmailContainIllegalAfterAPointCharacters(){
        assertThrows(InvalidCharacters.class,
                ()->customer.setEmail("omar.yatim@gmail.!com"),
                "the e-mail contains illegal character After (Point).");
    }
    private void checkEmailContainIllegalSetOfCharactersBeforeAt(){
        assertThrows(InvalidLengthStringException.class,
                ()->customer.setEmail("yatim@gmail.com"),
                "the e-mail contains illegal set of characters before @");
    }
    private void checkEmailContainIllegalSetOfCharactersAfterAt(){
        assertThrows(InvalidLengthStringException.class,
                ()->customer.setEmail("omar.yatim@il.com"),
                "the e-mail contains illegal set of  character After @");
    }
    private void checkEmailContainIllegalSetOfCharactersAfterPoint(){
        assertThrows(InvalidLengthStringException.class,
                ()->customer.setEmail("omar.yatim@gmail.m"),
                "the e-mail contains illegal set of character After (Point).");
    }
    private void checkEmailNotNull(){
        assertThrows(NullValueException.class,
                ()->customer.setEmail(null),
                "the last name can't be NULL");
    }
    private void checkEmailWithoutAtCharacter(){
        assertThrows(InvalidEmailException.class,
                ()->customer.setEmail("omaryateemgmail.com"),
                "Email must contains @ character ");
    }
    private void checkEmailWithoutPointCharacter(){
        assertThrows(InvalidEmailException.class,
                ()->customer.setEmail("omaryateem@gmailcom"),
                "Email must contains (.) character ");
    }
    private void checkEmailValidForm(){
        assertThrows(InvalidEmailException.class,
                ()->customer.setEmail("omar.yateem@gmailcom"),
                "(.) character must come after @ character");
    }
    private void checkEmailGetterAndSetter(){
        customer.setEmail("omaryateem@hotmail.com");
        assertEquals("omaryateem@hotmail.com",
                customer.getEmail(),
                "The getter and Setter for last name don't work perfectly");
    }

    @Test
    @DisplayName("Telephone Getter and Setter")
    void TestTelephone() {
        checkTelephoneMinLength();
        checkTelephoneMaxLength();
        checkTelephoneContainLegalCharacters();
        checkTelephoneNotNull();
        checkTelephoneGetterAndSetter();
    }
    private void checkTelephoneMinLength(){
        assertThrows(InvalidLengthStringException.class,
                ()->customer.setTelephone("12"),
                "Telephone must at least contains 7 characters");
    }
    private void checkTelephoneMaxLength(){
        assertThrows(InvalidLengthStringException.class,
                ()->{
                    String s ="";
                    for (int i = 0; i < 17; i++) {
                        s += "1";
                    }
                    customer.setTelephone(s);
                },
                "Telephone must not contains more than 16 characters");
    }
    private void checkTelephoneContainLegalCharacters(){
        assertThrows(InvalidCharacters.class,
                ()->customer.setTelephone("0122d3345"),
                "Telephone can't contains illegal characters");
    }
    private void checkTelephoneNotNull(){
        assertThrows(NullValueException.class,
                ()->customer.setTelephone(null),
                "the Telephone can't be NULL");
    }
    private void checkTelephoneGetterAndSetter(){
        customer.setTelephone("0123456");
        assertEquals("0123456",
                customer.getTelephone(),
                "The getter and Setter for Telephone don't work perfectly");
    }

    @Test
    @DisplayName("Address Id Getter and Setter")
    void setAddressID() {
        checkAddressIdZero();
        checkAddressIdNegativeVale();
        checkAddressIDGetterAndSetter();
    }
    private void checkAddressIdZero(){
        assertThrows(InvalidNumberException.class,
                ()->customer.setAddressID(0),
                "The ID of the address can't be Negative");
    }
    private void checkAddressIdNegativeVale(){
        assertThrows(InvalidNumberException.class,
                ()->customer.setAddressID(-1),
                "The ID of the address can't be Negative");
    }
    private void checkAddressIDGetterAndSetter(){
        customer.setAddressID(1);
        assertEquals(1,
                customer.getAddressID(),
                "The ID Customer getter and setter don't work perfectly");
    }
    @AfterEach
    void destroy(){
        customer = null;
    }
}