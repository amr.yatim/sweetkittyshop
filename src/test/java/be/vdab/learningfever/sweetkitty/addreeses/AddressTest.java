package be.vdab.learningfever.sweetkitty.addreeses;

import be.vdab.learningfever.sweetkitty.exceptions.InvalidLengthStringException;
import be.vdab.learningfever.sweetkitty.exceptions.InvalidNumberException;
import be.vdab.learningfever.sweetkitty.exceptions.NullValueException;
import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class AddressTest {
    private Address address;

    @BeforeEach
    void init(){
        address = new Address(
                "Brugse SteenWeg",
                183,
                "B",
                "9990",
                "Maldegem",
                "Belgie"
        );
        address.setAddressID(1);
    }

    @Test
    void testInitialAddressID(){
        //checkAddressIdNotZero();
        checkAddressIdNegativeVale();
        checkAddressIDGetterAndSetter();
    }
    private void checkAddressIdNotZero(){
        assertThrows(InvalidNumberException.class,
                ()->address.setAddressID(0),
                "The ID of the address can't be ZERO");
    }
    private void checkAddressIdNegativeVale(){
        assertThrows(InvalidNumberException.class,
                ()->address.setAddressID(-1),
                "The ID of the address can't be Negative");
    }
    private void checkAddressIDGetterAndSetter(){
        address.setAddressID(1);
        assertEquals(1,address.getAddressID(),"The ID Address getter and setter don't work perfectly");
    }

    @Test
    @DisplayName("Street Getter And Setter Testing")
    void TestStreetGetterAndSetter(){
     checkStreetLength();
     checkStreetNotNull();
     checkStreetGetterAndSetter();
    }

    private void checkStreetLength(){
        assertThrows(InvalidLengthStringException.class,
                ()->address.setStreet("AB"),
                "The street name must at least contains 3 characters");
    }
    private void checkStreetNotNull(){
        assertThrows(NullValueException.class,
                ()->address.setStreet(null),
                "the street name can't be NULL");
    }
    private void checkStreetGetterAndSetter(){
        address.setStreet("Nieuwe Leven");
        assertEquals("Nieuwe Leven",
                address.getStreet(),
                "The Street getter and Setter don't work perfectly");
    }

    @Test
    @DisplayName("House Number Getter and Setter Testing")
    void testHouseNumberGetterAndSetter(){
        checkHouseNumberZeroValue();
        checkHouseNumberNegativeValue();
        checkHouseNumberGetterAndSetter();
    }

    private void checkHouseNumberZeroValue(){
        assertThrows(InvalidNumberException.class,
                ()->address.setHouseNumber(0),
                "House Number can't be Zero");
    }
    private void checkHouseNumberNegativeValue(){
        assertThrows(InvalidNumberException.class,
                ()->address.setHouseNumber(-3),
                "House Number can't be Negative");
    }
    private void checkHouseNumberGetterAndSetter(){
        address.setHouseNumber(5);
        assertEquals(5,
                address.getHouseNumber(),
                "House Number getter and Setter don't work perfectly");
    }

    @Test
    @DisplayName("House Bus Getter and setter Checking")
    void testHouseBusGetterAndSetter(){

        checkHouseBusLengthIsZero();
        checkHouseBusMaxLength();
        checkHouseBusNullValue();
        checkHouseBusGetterAndSetter();
    }

    private void checkHouseBusLengthIsZero(){
        assertThrows(InvalidLengthStringException.class,
                ()->address.setBus(""),
                "The house Bus can't contain empty String");
    }
    private void checkHouseBusMaxLength(){
        System.out.println("123456".length());
        assertThrows(InvalidLengthStringException.class,
                ()->address.setBus("123456"),
                "The  house Bus can't string with more than five characters");
    }
    private void checkHouseBusNullValue(){
        address.setBus(null);
        assertNull(address.getBus(),"The House address can't be set to null");
    }
    private void checkHouseBusGetterAndSetter(){
        address.setBus("B");
        assertEquals("B",
                address.getBus(),
                "The House Bus getter and setter don't work perfectly");
    }

    @Test
    @DisplayName("ZipCode Getter And Setter testing")
    void testZipCodeGetterAndSetter(){
        checkZipCodeLengthIsZero();
        checkZipCodeMaxLength();
        checkZipCodeNullValue();
        checkZipCodeGetterAndSetter();
    }

    private void checkZipCodeLengthIsZero(){
        assertThrows(InvalidLengthStringException.class,
                ()->address.setZipCode(""),
                "The ZipCode can't contain empty String");
    }
    private void checkZipCodeMaxLength(){
        assertThrows(InvalidLengthStringException.class,
                ()->address.setZipCode("12345678900"),
                "The  house Bus can't string with more than ten characters");
    }
    private void checkZipCodeNullValue(){
        address.setZipCode(null);
        assertNull(address.getZipCode(),"The ZipCode can't be set to null");
    }
    private void checkZipCodeGetterAndSetter(){
        address.setZipCode("9990");
        assertEquals("9990",
                address.getZipCode(),
                "The ZipCode getter and setter don't work perfectly");
    }

    @Test
    @DisplayName("City Getter and Setter testing")
    void testCityGetterAndSetter(){
        checkCityLengthZero();
        checkCityMaxLength();
        checkCityNotNull();
        checkCityGetterAndSetter();
    }

    private void checkCityLengthZero(){
        assertThrows(InvalidLengthStringException.class,
                ()->address.setCity(""),
                "The City name can't be empty");
    }
    private void checkCityMaxLength(){

        assertThrows(InvalidLengthStringException.class,
                ()->{
                    String s ="";
                    for (int i = 0; i < 151; i++) {
                        s += "1";
                    }
                    address.setCity(s);},
                "The City name can't contains more than 150 characters");
    }
    private void checkCityNotNull(){
        assertThrows(NullValueException.class,
                ()->address.setCity(null),
                "The City name can't be NULL");
    }
    private void checkCityGetterAndSetter(){
        address.setCity("GENT");
        assertEquals("GENT",
                address.getCity(),
                "The City getter and Setter don't work perfectly");
    }

    @Test
    @DisplayName("Country Getter And Setter testing")
    void testCountryGetterAndSetter(){
        checkCountryLengthZero();
        checkCountryMaxLength();
        checkCountryNotNull();
        checkCountryGetterAndSetter();
    }

    private void checkCountryLengthZero(){
        assertThrows(InvalidLengthStringException.class,
                ()->address.setCity(""),
                "The Country name can't be empty");
    }
    private void checkCountryMaxLength(){

        assertThrows(InvalidLengthStringException.class,
                ()->{
                    String s ="";
                    for (int i = 0; i < 101; i++) {
                        s += "1";
                    }
                    address.setCountry(s);},
                "The Country name can't contains more than 100 characters");
    }
    private void checkCountryNotNull(){
        assertThrows(NullValueException.class,
                ()->address.setCity(null),
                "The Country name can't be NULL");
    }
    private void checkCountryGetterAndSetter(){
        address.setCity("Belgium");
        assertEquals("Belgium",
                address.getCity(),
                "The Country getter and Setter don't work perfectly");
    }


    @AfterEach
    void destroy(){
        address = null;
    }

}