package be.vdab.learningfever.sweetkitty.addreeses.daos;

import be.vdab.learningfever.sweetkitty.addreeses.Address;
import be.vdab.learningfever.sweetkitty.addreeses.exception.AddressException;
import be.vdab.learningfever.sweetkitty.facade.JdbcFacade;
import org.apache.ibatis.jdbc.ScriptRunner;


import org.junit.jupiter.api.*;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StandardAddressDoaTest {
    private static final String MEM_PATH="src/test/resources/mainInfo.properties" ;
    private StandardAddressDoa sad;
    private List<Address> listAddress;

    @BeforeEach
    void init(){
        listAddress = new ArrayList<>();
        sad = new StandardAddressDoa(new JdbcFacade(MEM_PATH));
        Address address = new Address(
                "Fraiselaan",
                22,
                null,
                null,
                "Oostende",
                "België");
        address.setAddressID(1);
        listAddress.add(address);
        address = new Address(
                "Brugse Steenweg",
                183,
                "B",
                "9990" ,
                "Maldegem",
                "België"
                );
        address.setAddressID(2);
        listAddress.add(address);
        address = new Address(
                "Fraiselaan",
                1,
                "A",
                "9000",
                "Gent",
                "België"
                );
        address.setAddressID(3);
        listAddress.add(address);
        address = new Address(
                "Vleeshuisstraat",
                174,
                null,
                "9000",
                "Gent",
                "België"
                );
        address.setAddressID(4);
        listAddress.add(address);
        try(
                BufferedReader schemaReader = new BufferedReader(new FileReader("src/test/resources/Address_Schema.sql"));
                BufferedReader dataReader = new BufferedReader(new FileReader("src/test/resources/Address_Data.sql"))
        ){
            ScriptRunner runner = new ScriptRunner(sad.getFacade().getConnect());
            runner.runScript(schemaReader);
            runner.runScript(dataReader);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }

    @Test
    @DisplayName("Get Address By Id exists")
    void testGetAddressById(){
        Address acceptedAddress = listAddress.get(0);
        Address currentAddress = sad.getAddressById(acceptedAddress.getAddressID());
        assertEquals(acceptedAddress,currentAddress,"The Address are not equals");
    }

    @Test
    @DisplayName("Get Address By Id doesn't exists")
    void testGetAddressByIdDoesNotExists(){
        assertNull(sad.getAddressById(0),"It must return Null");
    }

    @Test
    @DisplayName("Get Addresses By ZipCode exists")
    void testGetAddressesByZipCode(){
        checkGetAddressesByZipCodeOneResult();
        checkGetAddressesByZipCodeNullValue();
        checkGetAddressesByZipCodeMultiResult();
        checkGetAddressesByZipCodeEmpty();
    }
    private void checkGetAddressesByZipCodeOneResult(){
        assertTrue(
                sad.getAddressesByZipCode(listAddress.get(1).getZipCode()).size()==1,
                "The one Address result by zipcode doesn't match");
    }
    private void checkGetAddressesByZipCodeNullValue(){
        assertEquals(listAddress.get(0),
                sad.getAddressesByZipCode(listAddress.get(0).getZipCode()).get(0),
                        "The one Address result with NULL zipcode doesn't match");
    }
    private void checkGetAddressesByZipCodeMultiResult(){
        Address[] addresses = {listAddress.get(2),listAddress.get(3)};
        assertArrayEquals(addresses,
                sad.getAddressesByZipCode(listAddress.get(2).getZipCode()).toArray(Address[]::new),
                "The array of addresses by zipcode don't match");
    }
    private void checkGetAddressesByZipCodeEmpty(){
        assertEquals(new ArrayList<Address>(),
                sad.getAddressesByZipCode("11111"),
                "Get Addresses by ZipCode doesn't exists, doesn't work perfectly");
    }

    @Test
    @DisplayName("Get Addresses By ZipCode don't exists")
    void testGetAddressesByZipCodeDoNotExists(){
        assertTrue(sad.getAddressesByZipCode("8000").size()==0,"The list must be empty");
    }

    @Test
    @DisplayName("Get Addresses By City exists")
    void testGetAddressesByCity(){
        checkGetAddressesByCityOneResult();
        checkGetAddressesByCityNullValue();
        checkGetAddressesByCityMultiResult();
    }

    private void checkGetAddressesByCityOneResult(){
        assertTrue(
                sad.getAddressesByCity(listAddress.get(1).getCity()).size()==1,
                "The one Address result by City doesn't match");
    }
    private void checkGetAddressesByCityNullValue(){
        assertTrue(sad.getAddressesByCity(null).size()==0,"The City shouldn't be NULL");
    }
    private void checkGetAddressesByCityMultiResult(){
        Address[] addresses = {listAddress.get(2),listAddress.get(3)};
        assertArrayEquals(addresses,
                sad.getAddressesByCity(listAddress.get(2).getCity()).toArray(Address[]::new),
                "The array of addresses by City don't match");
    }

    @Test
    @DisplayName("Get Addresses By City don't exists")
    void testGetAddressesByCityDoNotExists(){
        assertTrue(sad.getAddressesByCity("Talbisah").size()==0,"The list must be empty");
    }

    @Test
    @DisplayName("Get Addresses By Street exists")
    void testGetAddressesByStreet(){
        checkGetAddressesByStreetOneResult();
        checkGetAddressesByStreetNullValue();
        checkGetAddressesByStreetMultiResult();
    }

    private void checkGetAddressesByStreetOneResult(){
        assertTrue(
                sad.getAddressesByStreet(listAddress.get(3).getStreet()).size()==1,
                "The one Address result by Street doesn't match");
    }
    private void checkGetAddressesByStreetNullValue(){
        assertTrue(sad.getAddressesByStreet(null).size()==0,"The Street shouldn't be NULL");
    }
    private void checkGetAddressesByStreetMultiResult(){
        Address[] addresses = {listAddress.get(0),listAddress.get(2)};
        assertArrayEquals(addresses,
                sad.getAddressesByStreet(listAddress.get(2).getStreet()).toArray(Address[]::new),
                "The array of addresses by Street don't match");
    }

    @Test
    @DisplayName("Get Addresses By Street don't exists")
    void testGetAddressesByStreetDoNotExists(){
        assertTrue(sad.getAddressesByStreet("Talbisah").size()==0,"The list must be empty");
    }

    @Test
    @DisplayName("Update Addresses by address object")
    void testUpdateAddresses(){
        checkUpdateExistingAddress();
        checkUpdateNullAddress();
    }

    @Test
    @DisplayName("Insert Address into Addresses")
    void testInsertAddress(){
        checkInsertNewAddress();
        checkInsertNullAddress();
    }

    @Test
    @DisplayName("Delete Address By ID")
    void testDeleteAddressById(){
        checkDeleteById();
        checkDeleteByIdForZeroAndNegativeValue();
    }

    private void checkInsertNewAddress(){
        Address address = new Address(
                "Brugse Steenweg",
                183,
                "B",
                "9990",
                "Maladegem",
                "België");
        sad.insertAddress(address);
        address.setAddressID(5);
        assertEquals(address,
                sad.getAddressById(address.getAddressID()),
                "The insert doesn't work perfectly");
    }
    private void checkInsertNullAddress(){
        assertThrows(AddressException.class,
                ()->sad.insertAddress(null),
                "You can't insert NULL Address");
    }
    private void checkUpdateExistingAddress(){
        Address address = new Address(
                "Brugse Steenweg",
                183,
                "A",
                "9990",
                "Maladegem",
                "België");
        address.setAddressID(3);
        sad.updateAddress(address);
        assertEquals(address,sad.getAddressById(address.getAddressID()),"The Update doesn't work perfectly");
    }
    private void checkUpdateNullAddress(){
        assertThrows(AddressException.class,
                ()->sad.updateAddress(null),
                "You can't insert NULL Address");
    }
    private void checkDeleteById(){
        sad.deleteById(1);
        assertNull(sad.getAddressById(1),"The Delete doesn't work perfectly");
    }
    private void checkDeleteByIdForZeroAndNegativeValue(){
        assertThrows(AddressException.class,
                ()->sad.deleteById(0),
                "There is no zero record to delete");
    }


    @AfterEach
    void destroy(){
        try {
            sad.getFacade().close();
        }catch (Exception e){
            e.printStackTrace();
        }
        listAddress.clear();
        listAddress = null;
        sad = null;
    }

}