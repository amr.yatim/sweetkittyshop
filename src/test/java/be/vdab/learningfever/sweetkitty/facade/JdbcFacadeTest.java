package be.vdab.learningfever.sweetkitty.facade;

import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.*;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.*;

class JdbcFacadeTest {

    public static final String MEM_PATH ="src/test/resources/mainInfo.properties";
    private JdbcFacade jFacade;

    @BeforeEach
    void init(){
        jFacade = new JdbcFacade(MEM_PATH);
        try(
                BufferedReader schemaReader = new BufferedReader(new FileReader("src\\test\\resources\\Schema.sql"));
                BufferedReader dataReader = new BufferedReader(new FileReader("src\\test\\resources\\Data.sql"))
        ){
            ScriptRunner runner = new ScriptRunner(jFacade.getConnect());
            runner.runScript(schemaReader);
            runner.runScript(dataReader);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }



    @Test
    @DisplayName("Test URL")
    void getURLTest(){
        String acceptedURL = "jdbc:hsqldb:mem:mymemdb";
        assertEquals(acceptedURL,jFacade.getURL(),"They don't give the same URL");
    }

    @Test
    @DisplayName("Test User name")
    void getUserTest(){
        assertEquals("sa",jFacade.getUser(),"They don't have the same UserName");
    }

    @Test
    @DisplayName("Test Password")
    void getPasswordTest(){
        assertEquals("",jFacade.getPassword(),"They don't have the same Password");
    }

    @Test
    @DisplayName("Statement Test")
    void getDefaultStatementTest(){
        checkDefaultResultSetType();
        checkDefaultResultSetConcurrency();
    }

    private void checkDefaultResultSetType(){
        try {
            assertEquals(JdbcFacade.TYPE_STANDARD,
                    jFacade.getStatement().getResultSetType(),
                    "They Don't have the same ResultSet Type");
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
    private void checkDefaultResultSetConcurrency(){
        try{
            assertEquals(JdbcFacade.CONCUR_READ_ONLY,
                    jFacade.getStatement().getResultSetConcurrency(),
                    "They don't have the same Concurrency");
        }catch (SQLException e){
            e.printStackTrace();
        }
    }

    @Test
    @DisplayName("Update Using executeUpdate methods")
    void executeUpdateTest(){
        float acceptedPrice = 3.6f;
        jFacade.executeUpdate("UPDATE Beers SET Price=3.6 WHERE Id=2");
        float currentPrice = 0.0f;
        try{
            ResultSet set = jFacade.executeQuery("SELECT Price FROM Beers WHERE Id=2");
            if(set.next()) {
                currentPrice = set.getFloat("Price");
            }else{
                System.out.println("NO RESULT WHAT THE FUCK !!!");
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        assertEquals(acceptedPrice,currentPrice,"executeUpdate methods doesn't work correctly");
    }

    @AfterEach
    void destroy(){
        try {
            jFacade.close();
        }catch (Exception e){
            System.out.println("Can't be closed!!!");
            e.printStackTrace();
        }
    }

}