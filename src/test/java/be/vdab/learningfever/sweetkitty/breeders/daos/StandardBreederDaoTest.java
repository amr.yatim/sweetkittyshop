package be.vdab.learningfever.sweetkitty.breeders.daos;

import be.vdab.learningfever.sweetkitty.breeders.Breeder;
import be.vdab.learningfever.sweetkitty.breeders.exceptions.BreederException;
import be.vdab.learningfever.sweetkitty.facade.JdbcFacade;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StandardBreederDaoTest {
    private static final String MEM_PATH="src/test/resources/mainInfo.properties" ;
    StandardBreederDao sbd;
    List<Breeder> listBreeder;

    @BeforeEach
    void init(){
        listBreeder = new ArrayList<>();
        sbd = new StandardBreederDao(new JdbcFacade(MEM_PATH));

        Breeder breeder = new Breeder();
        breeder.setBreederId(1);
        breeder.setCompanyName("Black Mystic Legend");
        breeder.setCustomerId(8);
        breeder.setAddressId(8);
        listBreeder.add(breeder);

        breeder = new Breeder();
        breeder.setBreederId(2);
        breeder.setCompanyName("Kitty wizzy");
        breeder.setCustomerId(9);
        breeder.setAddressId(9);
        listBreeder.add(breeder);

        breeder = new Breeder();
        breeder.setBreederId(3);
        breeder.setCompanyName("Ankeshof");
        breeder.setCustomerId(10);
        breeder.setAddressId(10);
        listBreeder.add(breeder);

        breeder = new Breeder();
        breeder.setBreederId(4);
        breeder.setCompanyName("Lieve Poezies");
        breeder.setCustomerId(11);
        breeder.setAddressId(11);
        listBreeder.add(breeder);

        breeder = new Breeder();
        breeder.setBreederId(5);
        breeder.setCompanyName("Katia s Palazo");
        breeder.setCustomerId(10);
        breeder.setAddressId(12);
        listBreeder.add(breeder);

        breeder = new Breeder();
        breeder.setBreederId(6);
        breeder.setCompanyName("Kitty wizzy");
        breeder.setCustomerId(13);
        breeder.setAddressId(13);
        listBreeder.add(breeder);

        breeder = new Breeder();
        breeder.setBreederId(7);
        breeder.setCompanyName("Groot Kathof");
        breeder.setCustomerId(14);
        breeder.setAddressId(12);
        listBreeder.add(breeder);


        try(
                BufferedReader schemaReader = new BufferedReader( new FileReader("src/test/resources/breeders_Schema.sql"));
                BufferedReader dataReader = new BufferedReader(new FileReader("src/test/resources/Breeders_Data.sql"))
                ){
            ScriptRunner runner = new ScriptRunner(sbd.getFacade().getConnect());
            runner.runScript(schemaReader);
            runner.runScript(dataReader);
        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }

    @Test
    @DisplayName("test get breeder by id exists")
    void testBreederByIdExists() {
        assertEquals(
                listBreeder.get(0),
                sbd.getBreederById(listBreeder.get(0).getBreederId()),
                "get breeder by id exists doesn't work perfectly"
        );
    }
    @Test
    @DisplayName("test get breeder by id not exists")
    void testBreederByIdNotExists() {
        assertNull(
                sbd.getBreederById(20),
                "get breeder by id not exists , doesn't work perfectly"
        );
    }

    @Test
    @DisplayName("test get breeders by customer id (one result)")
    void testBreedersByCustomerIdOneResult() {
        assertTrue(
                sbd.getBreedersByCustomerId(listBreeder.get(1).getCustomerId()).size()==1,
                "test get breeders by customer id (one result),doesn't work perfectly");
    }
    @Test
    @DisplayName("test get breeders by customer id (multi result)")
    void getBreedersByCustomerIdMultiResult(){
        Breeder[] list = {listBreeder.get(2),listBreeder.get(4)};
        assertArrayEquals(
                list,
                sbd.getBreedersByCustomerId(listBreeder.get(2).getCustomerId()).toArray(Breeder[]::new),
                "test get breeders by customer id (multi result), doesn't work perfectly");
    }
    @Test
    @DisplayName("test get breeders by customer id (no result)")
    void TestBreedersByCustomerIdNoResult() {
        assertEquals(
                new ArrayList<Breeder>(),
                sbd.getBreedersByCustomerId(30),
                "test get breeders by customer id (no result), doesn't work perfectly");
    }


    @Test
    @DisplayName("test get breeders by address id (one result)")
    void testBreedersByAddressIdOneResult() {
        assertTrue(
                sbd.getBreedersByAddressId(listBreeder.get(2).getAddressId()).size()==1,
                "test get breeders by address id (one result),doesn't work perfectly");
    }
    @Test
    @DisplayName("test get breeders by address id (multi result)")
    void getBreedersByAddressIdMultiResult() {
        Breeder[] list = {listBreeder.get(4),listBreeder.get(6)};
        assertArrayEquals(
                list,
                sbd.getBreedersByAddressId(listBreeder.get(4).getAddressId()).toArray(Breeder[]::new),
                "test get breeders by address id (multi result), doesn't work perfectly");
    }
    @Test
    @DisplayName("test get breeders by address id (no result)")
    void TestBreedersByAddressIdNoResult() {
        assertEquals(
                new ArrayList<Breeder>(),
                sbd.getBreedersByAddressId(30),
                "test get breeders by Address id (no result), doesn't work perfectly");
    }

    @Test
    @DisplayName("test get breeders by company name id (one result)")
    void testBreedersByCompanyNameOneResult() {
        assertTrue(
                sbd.getBreedersByCompanyName(listBreeder.get(3).getCompanyName()).size()==1,
                "test get breeders by company name (one result),doesn't work perfectly");
    }
    @Test
    @DisplayName("test get breeders by company name (multi result)")
    void getBreedersByCompanyNameMultiResult() {
        Breeder[] list = {listBreeder.get(1),listBreeder.get(5)};
        assertArrayEquals(
                list,
                sbd.getBreedersByCompanyName(listBreeder.get(1).getCompanyName()).toArray(Breeder[]::new),
                "test get breeders by Company Name(multi result), doesn't work perfectly");
    }
    @Test
    @DisplayName("test get breeders by Company Name (no result)")
    void TestBreedersByCompanyNameNoResult() {
        assertEquals(
                new ArrayList<Breeder>(),
                sbd.getBreedersByCompanyName("KITTYCATKITTY"),
                "test get breeders by company name (no result), doesn't work perfectly");
    }

    @Test
    @DisplayName("update breeders")
    void updateBreeders() {
        Breeder breeder = listBreeder.get(5);
        breeder.setCompanyName("Kitty Cat");
        breeder.setCustomerId(16);
        breeder.setAddressId(17);
        sbd.updateBreeders(breeder);
        assertEquals(
                breeder,
                sbd.getBreederById(breeder.getBreederId()),
                "test update breeder doesn't work perfectly"
        );
    }
    @Test
    @DisplayName("update breeders(Null)")
    void updateBreedersNull() {
        assertThrows(
                BreederException.class,
                ()->sbd.updateBreeders(null),
                "update breeder isn't possible by null"
        );
    }

    @Test
    @DisplayName("insert breeder")
    void insertBreeder() {
        Breeder breeder = new Breeder();
        breeder.setCompanyName("Kitty Cat");
        breeder.setAddressId(16);
        breeder.setCustomerId(10);
        breeder.setBreederId(8);
        sbd.insertBreeder(breeder);
        assertNotNull(sbd.getBreederById(8),"insert breeder doesn't work perfectly");
    }
    @Test
    @DisplayName("insert breeder(null)")
    void insertBreederNull() {
        assertThrows(
                BreederException.class,
                ()->sbd.insertBreeder(null),
                "insert breeder is not possible by null"
        );
    }

    @Test
    @DisplayName("delete breeder")
    void deleteBreeder() {
        sbd.deleteBreeder(1);
        assertNull(
                sbd.getBreederById(1),
                "delete breeder doesn't work perfectly");
    }
    @Test
    @DisplayName("delete breeder(Zero)")
    void deleteBreederZero() {
        assertThrows(
                BreederException.class,
                ()->sbd.deleteBreeder(0),
                "delete breeder is not possible by zero or negative number"
        );
    }

    @AfterEach
    void destroy(){
        try{
            sbd.getFacade().close();
        }catch (Exception e){
            e.printStackTrace();
        }
        sbd = null;
        listBreeder.clear();
        listBreeder = null;
    }
}