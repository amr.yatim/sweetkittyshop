package be.vdab.learningfever.sweetkitty.breeders;

import be.vdab.learningfever.sweetkitty.exceptions.InvalidLengthStringException;
import be.vdab.learningfever.sweetkitty.exceptions.InvalidNumberException;
import be.vdab.learningfever.sweetkitty.exceptions.NullValueException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BreederTest {
    Breeder breeder;

    @BeforeEach
    void init(){
        breeder = new Breeder();
    }

    @Test
    @DisplayName("test breeder is setter and getter")
    void testBreederId() {
        checkBreederIdNegative();
        checkBreederIdSetterAndGetter();
    }
    private void checkBreederIdNegative(){
        assertThrows(
                InvalidNumberException.class,
                ()->breeder.setBreederId(-1),
                "Breeder ID can't be Negative");
    }
    private void checkBreederIdSetterAndGetter(){
        breeder.setBreederId(1);
        assertEquals(
                1,
                breeder.getBreederId(),
                "Breeder ID getter and setter don't work perfectly"
        );
    }

    @Test
    @DisplayName("test address id setter and getter")
    void testAddressId() {
        checkAddressIdZero();
        checkAddressIdNegative();
        checkAddressIdSetterAndGetter();
    }
    private void checkAddressIdZero(){
        assertThrows(
                InvalidNumberException.class,
                ()->breeder.setAddressId(0),
                "Address ID can't be Zero"
        );

    }
    private void checkAddressIdNegative(){
        assertThrows(
                InvalidNumberException.class,
                ()->breeder.setAddressId(-1),
                "Address ID can't be Negative"
        );
    }
    private void checkAddressIdSetterAndGetter(){
        breeder.setAddressId(1);
        assertEquals(
                1,
                breeder.getAddressId(),
                "Address ID setter and getter don't work perfectly"
        );
    }

    @Test
    @DisplayName("test company name setter and getter")
    void testCompanyName() {
        checkCompanyNameNull();
        checkCompanyNameMinLength();
        checkCompanyNameMaxLength();
        checkCompanyNameSetterAndGetter();
    }
    private void checkCompanyNameNull(){
        assertThrows(
                NullValueException.class,
                ()->breeder.setCompanyName(null),
                "Company Name can'y be NULL"
        );

    }
    private void checkCompanyNameMinLength(){
        assertThrows(
                InvalidLengthStringException.class,
                ()->breeder.setCompanyName("A"),
                "Company Name must contain at least 2 characters"
        );
    }
    private void checkCompanyNameMaxLength(){
        assertThrows(
                InvalidLengthStringException.class,
                ()->{
                    String s="";
                    for (int i = 0; i < 101; i++) {
                        s +="s";
                    }
                    breeder.setCompanyName(s);
                },
                "Company Name must not contain more than 100 characters"
        );
    }
    private void checkCompanyNameSetterAndGetter(){
        breeder.setCompanyName("VDAB");
        assertEquals(
                "VDAB",
                breeder.getCompanyName(),
                "Company Name getter and setter don't perfectly"
        );
    }

    @Test
    @DisplayName("test customer id setter and getter")
    void testCustomerId() {
        checkCustomerIdZero();
        checkCustomerIdNegative();
        checkCustomerIdSetterAndGetter();
    }
    private void checkCustomerIdZero(){
        assertThrows(
                InvalidNumberException.class,
                ()->breeder.setCustomerId(0),
                "Customer ID can't be Zero"
        );
    }
    private void checkCustomerIdNegative(){
        assertThrows(
                InvalidNumberException.class,
                ()->breeder.setCustomerId(-1),
                "Customer ID can't be Negative"
        );
    }
    private void checkCustomerIdSetterAndGetter(){
        breeder.setCustomerId(1);
        assertEquals(
                1,
                breeder.getCustomerId(),
                "Customer ID getter and setter don't work perfectly"
        );
    }
    @AfterEach
    void destroy(){
        breeder = null;
    }
}