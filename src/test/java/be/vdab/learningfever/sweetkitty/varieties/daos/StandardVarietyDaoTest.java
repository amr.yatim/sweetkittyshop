package be.vdab.learningfever.sweetkitty.varieties.daos;

import be.vdab.learningfever.sweetkitty.facade.JdbcFacade;
import be.vdab.learningfever.sweetkitty.orders.exceptions.OrderException;
import be.vdab.learningfever.sweetkitty.varieties.Variety;
import be.vdab.learningfever.sweetkitty.varieties.exceptions.VarietyException;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class StandardVarietyDaoTest {
    private static final String MEM_PATH="src/test/resources/mainInfo.properties" ;
    StandardVarietyDao svd;
    List<Variety> listVarieties;

    @BeforeEach
    void init(){
        listVarieties = new ArrayList<>();
        svd= new StandardVarietyDao(new JdbcFacade(MEM_PATH));

        Variety type = new Variety();
        type.setVarietyId(1);
        type.setVarietyName("Heilige Birmaan");
        type.setDescription("Heilige Birmaan kat");
        type.setUnitPrice(400.0);
        listVarieties.add(type);

        type = new Variety();
        type.setVarietyId(2);
        type.setVarietyName("Britse Korthaar");
        type.setDescription("Britse Korthaar kat");
        type.setUnitPrice(250.0);
        listVarieties.add(type);

        type = new Variety();
        type.setVarietyId(3);
        type.setVarietyName("Europese Korthaar");
        type.setDescription("Europese Korthaar kat");
        type.setUnitPrice(50.0);
        listVarieties.add(type);

        type = new Variety();
        type.setVarietyId(4);
        type.setVarietyName("Naakt");
        type.setDescription("Naakt kat");
        type.setUnitPrice(375.0);
        listVarieties.add(type);

        type = new Variety();
        type.setVarietyId(5);
        type.setVarietyName("Perzische");
        type.setDescription("Perzische kat");
        type.setUnitPrice(830.0);
        listVarieties.add(type);

        type = new Variety();
        type.setVarietyId(6);
        type.setVarietyName("Naakt");
        type.setDescription("fake kat");
        type.setUnitPrice(50.0);
        listVarieties.add(type);

        try(
                BufferedReader schemaReader = new BufferedReader(new FileReader("src/test/resources/Variety_Schema.sql"));
                BufferedReader dataReader = new BufferedReader(new FileReader("src/test/resources/Variety_Data.sql"));
                ){
            ScriptRunner runner = new ScriptRunner(svd.getFacade().getConnect());
            runner.runScript(schemaReader);
            runner.runScript(dataReader);

        }catch (IOException ioe){
            ioe.printStackTrace();
        }
    }

    @Test
    @DisplayName("test get variety by varietyId ")
    void testVarietyById() {
        checkGetVarietyByIdExists();
        checkGetVarietyByIdNotExists();
    }

    private void checkGetVarietyByIdExists(){
        assertEquals(
                listVarieties.get(0),
                svd.getVarietyById(listVarieties.get(0).getVarietyId()),
                "get variety by id exists doesn't work perfectly");
    }
    private void checkGetVarietyByIdNotExists(){
        assertNull(svd.getVarietyById(10),
                "get variety by id not exists, doesn't work perfectly");
    }

    @Test
    @DisplayName("test Get Variety By Name One Result")
    void testGetVarietyByNameOneResult() {
        assertEquals(listVarieties.get(1),
                svd.getVarietiesByName(listVarieties.get(1).getVarietyName()).get(0),
                "Get variety by name One Result doesn't work perfectly");
    }

    @Test
    @DisplayName("Get varieties by Name Multi Result")
    void testGetVarietyByNameMultiResult() {
        Variety[] types = {listVarieties.get(3),listVarieties.get(5)};
        assertArrayEquals(types,
                svd.getVarietiesByName(listVarieties.get(3).getVarietyName()).toArray(Variety[]::new),
                "Get varieties by name Multi Result don't work perfectly");
    }

    @Test
    @DisplayName("Get variety by name doesn't exists")
    void testGetVarietyByNameNoResult() {
        assertEquals(new ArrayList<Variety>(),
                svd.getVarietiesByName("Kitty"),
                "Get variety by name doesn't exists, doesn't work perfectly");
    }

    @Test
    void getVarietiesByUnitPrice() {
    }

    @Test
    @DisplayName("test Get Variety By UnitPrice One Result")
    void testGetVarietyByUnitPriceOneResult() {
        assertEquals(listVarieties.get(0),
                svd.getVarietiesByUnitPrice(listVarieties.get(0).getUnitPrice()).get(0),
                "Get variety by unit price One Result doesn't work perfectly");
    }

    @Test
    @DisplayName("Get varieties by UnitPrice Multi Result")
    void testGetVarietyByUnitPriceMultiResult() {
        Variety[] types = {listVarieties.get(2),listVarieties.get(5)};
        assertArrayEquals(types,
                svd.getVarietiesByUnitPrice(listVarieties.get(2).getUnitPrice()).toArray(Variety[]::new),
                "Get varieties by unit price Multi Result don't work perfectly");
    }

    @Test
    @DisplayName("Get variety by UnitPrice doesn't exists")
    void testGetVarietyByUnitPriceNoResult() {
        assertEquals(new ArrayList<Variety>(),
                svd.getVarietiesByUnitPrice(1.0),
                "Get variety by unit price doesn't exists, doesn't work perfectly");
    }

    @Test
    @DisplayName("test update variety")
    void updateVarieties() {
        Variety type = listVarieties.get(5);
        type.setVarietyName("Kitty");
        type.setDescription("small cat");
        type.setUnitPrice(1000.0);
        svd.updateVarieties(type);
        assertEquals(
                type,
                svd.getVarietyById(type.getVarietyId()),
                "update varieties doesn't work perfectly"
        );
    }
    @Test
    @DisplayName("test update variety(null) ")
    void updateVarietyNull() {
        assertThrows(
                VarietyException.class,
                ()->svd.updateVarieties(null),
                "update variety isn't possible by Nll"
        );
    }
    @Test
    @DisplayName("test insert variety")
    void insertVariety() {
        Variety type = new Variety();
        type.setVarietyName("Kitty");
        type.setDescription("small cat");
        type.setUnitPrice(1000.0);
        type.setVarietyId(7);
        svd.insertVariety(type);
        assertEquals(
                type,
                svd.getVarietyById(7),
                "insert varieties doesn't work perfectly"
        );
        //assertNotNull(svd.getVarietyById(7),"insert varieties doesn't work perfectly");
    }
    @Test
    @DisplayName("test insert variety(null) ")
    void insertVarietyNull() {
        assertThrows(
                VarietyException.class,
                ()->svd.insertVariety(null),
                "insert variety isn't possible by Nll"
        );
    }

    @Test
    @DisplayName("test delete variety")
    void deleteVariety() {
        svd.deleteVariety(6);
        assertNull(svd.getVarietyById(6),"delete variety doesn't work perfectly");
    }
    @Test
    @DisplayName("test delete variety(zero) ")
    void deleteVarietyZero() {
        assertThrows(
                VarietyException.class,
                ()->svd.deleteVariety(0),
                "delete variety isn't possible by zero or negative number"
        );
    }

    @AfterEach
    void destroy(){
        try{
            svd.getFacade().close();
        }catch (Exception e){
            e.printStackTrace();
        }
        listVarieties.clear();
        listVarieties=null;
        svd = null;
    }
}