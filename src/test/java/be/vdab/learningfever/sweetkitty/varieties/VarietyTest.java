package be.vdab.learningfever.sweetkitty.varieties;

import be.vdab.learningfever.sweetkitty.exceptions.InvalidLengthStringException;
import be.vdab.learningfever.sweetkitty.exceptions.InvalidNumberException;
import be.vdab.learningfever.sweetkitty.exceptions.NullValueException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class VarietyTest {
    Variety type;

    @BeforeEach
    void init(){
       type = new Variety();
    }

    @Test
    @DisplayName("test variety id getter and setter")
    void testVarietyId() {
        checkVarietyIdNegative();
        checkVarietyIdGetterAndSetter();
    }
    private void checkVarietyIdNegative(){
        assertThrows(InvalidNumberException.class,
                ()->type.setVarietyId(-1),
                "Variety id can't have a negative value");
    }
    private void checkVarietyIdGetterAndSetter(){
        type.setVarietyId(1);
        assertEquals(
                1,
                type.getVarietyId(),
                "Variety id getter and setter don't work perfectly");
    }

    @Test
    @DisplayName("test variety name getter and setter")
    void testVarietyName() {
        checkVarietyNameMinLength();
        checkVarietyNameMaxLength();
        checkVarietyNameNullValue();
        checkVarietyNameGetterAndSetter();
    }
    private void checkVarietyNameMinLength(){
        assertThrows(
                InvalidLengthStringException.class,
                ()->type.setVarietyName("ab"),
                "Variety name must contain 3 characters at least"
        );
    }
    private void checkVarietyNameMaxLength(){
        assertThrows(
                InvalidLengthStringException.class,
                ()->{
                    String s="";
                    for (int i = 0; i < 46; i++) {
                        s += "s";
                    }
                    type.setVarietyName(s);
                },
                "Variety name must not contain more than 45 characters");
    }
    private void checkVarietyNameNullValue(){
        assertThrows(
                NullValueException.class,
                ()->type.setVarietyName(null),
                "Variety name can't be null");
    }
    private void checkVarietyNameGetterAndSetter(){
        type.setVarietyName("kitty");
        assertEquals(
                "kitty",
                type.getVarietyName(),
                "Variety name setter and getter don't work perfectly");
    }

    @Test
    @DisplayName("test variety description getter and setter")
    void testDescription() {
        checkVarietyDescriptionNullValue();
        checkVarietyDescriptionGetterAndSetter();
    }
    private void checkVarietyDescriptionNullValue(){
        assertThrows(
                NullValueException.class,
                ()->type.setDescription(null),
                "VarietyDescription can't be null"
        );
    }
    private void checkVarietyDescriptionGetterAndSetter(){
        String acceptedDescription = "kitty is a small cat";
        type.setDescription(acceptedDescription);
        assertEquals(
                acceptedDescription,
                type.getDescription(),
                "Variety description getter and setter don't work perfectly");
    }

    @Test
    @DisplayName("test variety unit price getter and setter")
    void testUnitPrice() {
        checkUnitPriceZero();
        checkUnitPriceNegative();
        checkUnitPriceGetterAndSetter();
    }
    private void checkUnitPriceZero(){
        assertThrows(
                InvalidNumberException.class,
                ()->type.setUnitPrice(0.0),
                "Variety unit price can't be Zero"
        );
    }
    private void checkUnitPriceNegative(){
        assertThrows(
                InvalidNumberException.class,
                ()->type.setUnitPrice(-1.0),
                "variety unit price can't be negative"
        );
    }
    private void checkUnitPriceGetterAndSetter(){
        double price = 1.5;
        type.setUnitPrice(price);
        assertEquals(
                price,
                type.getUnitPrice(),
                "variety unit price doesn't work perfectly"
        );
    }

    @AfterEach
    void destroy(){
        type = null;
    }
}